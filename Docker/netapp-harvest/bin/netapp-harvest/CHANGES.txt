Discussion about NetApp Harvest, Graphite, and Grafana is found on NetApp Communities:
http://community.netapp.com/t5/OnCommand-Storage-Management-Software-Discussions/bd-p/oncommand-storage-management-software-discussions

################################################################################################################

Change log:

1.3 (10-Nov-2016)
-FEATURE: Added ability to use multiple monitoring templates. The list is comma or whitespace separated and each template will be loaded and merged
          in the order it was listed. Merging is done for 1st level keys, so if you want to modify something you must define the entire key value. For example
          to use the default monitoring template but disable collection of luns simply create a new monitoring template like lun-disable.conf with a lun section
          where enabled => 0. Then set the poller with "template = default, lun-disable.conf" and the effective config will be the default with lun collection disabled
-FEATURE: Added logfile rotation feature whereby filesize is checked and if too large it rotated with a .log.# filename extension.  Controlled by poller
          parameters logfile_rotate_mb (default 5) and logfile_rotate_keep (default 4) resulting in max ~20 MB logs per poller.
-FEATURE: Added -confdir and -logdir to netapp-worker and netapp-manager to allow specification of alternative directories
-FEATURE: Added and updated some graphs, tables, and singlestat panels to improve dashboard usability.  Now require minimum Grafana v3.0
-FEATURE: NFS v4 and v4.1 support added; counters + new rows in SVM and node dashboards.  Note: For cDOT 8.2 due to lack of
          other ops and latency counters related panels are incorrect or N/A.
-FEATURE: Host adapter (disk and tape HBA) read and write bytes support for cDOT and 7-mode added; counters added in default templates and updated
          panel in the node dashboard, and new row and panels in the disk and cache layers dashboard
-FEATURE: Added MetroCluster FCVI write mirroring IOPs, data, and latency for cDOT added; counters added in MetroCluster detail dashboard.
-FEATURE: Added copy offload statistic collection and visualization in cluster and SVM dashboards
-FEATURE: Enhanced the netapp-manager dashboard management feature with better exception handling and descriptive error messages
-FEATURE: Added template for OCUM 6.4 and additional volume metric snapshot-reserve-used-per-day
-FEATURE: Modified minimum latency required (latency_io_reqd) to apply to all counters of unit 'average' with 'latency' in the counter name
-FEATURE: Set netapp-worker process to run with UID and GID of the file permissions of netapp-worker file
-FEATURE: Added support for ONTAP 9, 9.1, OCUM 6.4, and OCUM 7.0
-FEATURE: Added per workload and policy group sequential read %
-FEATURE: Added latency_from_suspend to show latency due to 'other' suspend reasons
-CHANGED: Renamed poller parameter site to group; site is now depreciated.  Any occurrences of site will be aliased to group for
          compatibility but it is recommended to rename to group in the conf file
-FIXED:   Detect and handle non-ASCII characters more gracefully
-FIXED:   Allow duplicate name and uuid unless name uses the uuid format; disk:raid_group will now collect for example
-FIXED:   Updated init.d script (./util/netapp-harvest) to include #!/bin/bash as first line, correct exit code for status arg, and options for LOGDIR and CONFDIR
-FIXED:   Updated netapp-manager setting SSL_verify_mode to use constant (allows dashboard management with SSL and newer client SSL libraries)
-FIXED:   Fixed units of flashpool capacity counters 7-mode 8.1 and 8.2 using overrides [same as already done in cDOT]
-FIXED:   Modified warning message in case poller can't keep up with requested polling interval to be more clear and result in fewer log entries
-FIXED:   Added fix for nic_common counters to workaround bug 915637 and improve accuracy of port counters (8.3.2 & 8.2.4 template changes + cdot-nic-common plugin)
-FIXED:   Variable display_name in a poller config was non-operative and is now fixed.
-FIXED:   Updated instance logic for iscsi_lif to use net-interface-get-iter instead of perf APIs to avoid bug 764178 
-FIXED:   Updated lun-get-iter logic to discover only online and mapped LUNs 
          (avoids warning 'update of data cache failed with reason: For lun object, no instances were found to match the given query.')
-FIXED:   Updated workload instance logic to not collect mirrored but inactive workloads
-FIXED:   Fixed some singlestat panels in cDOT network port and lif dashboards
-FIXED:   In 8.1 cDOT template removed nic_common object type and volume:node_uuid as these do not exist in this release
-FIXED:   Removed read and write latency LUN counters from DOT 7.3 template as these did not exist in that release
-FIXED:   Updated netapp-manager import option to throw a warning if a file is not properly formatted JSON  and continue (instead of simply aborting)
-FIXED:   If clustername has '.' in it these will be replaced with '_' to avoid creating an undesired hierarchy in Graphite
-FIXED:   Prevent site names with . or spaces in them as these aren't handled well by Graphite. Recommend to use only A-Za-z0-9-_ characters.

1.2.2P1 (15-Sep-2015)
-FIXED:   Fixed handling and display of 7-mode cifs_stats statistics which are vFiler specific (plugin + windows fileserving dashboard)
-FEATURE: Created per vFiler avg latency roll-up for 7-mode cifs_domain statistics, updated 7-mode dashboards to use these stats

1.2.2 (31-Aug-2015)
-FIXED:   Fixed Graphite network socket reconnection logic mistake introduced in 1.2RC3 and added PIPE signal handler
-FIXED:   Fixed logic to print STDERR to logfile on Linux platforms
-FIXED:   Fixed netapp-manager to kill worker correctly if username contains non-word characters (ex: _ and -)

1.2.1 (25-Aug-2015)
-FIXED:   Included fix from 1.2RC3 related to plugin error and restart; it was inadvertently not merged previously
-FIXED:   Added validation in netapp-manager to check that site field is populated and without whitespace otherwise a warning
          is logged and the poller will not be managed
-FEATURE: Added default conf file for 8.0 release of 7-mode 

1.2 (19-Aug-2015)
-CHANGED: NMSDK 5.4 adds HTTP/1.1 support but the SDK invoke calls will fail with error 400 if reverse hostname resolution fails.  To
          workaround this issue netapp-worker, perf-counters-to-excel, & perf-counters-utility will set HTTP/1.0 if using an SDK that
          supports HTTP/1.1 and reverse hostname resolution fails.
-CHANGED: Modified netapp-manager -upload to be -import and -download to be -export to be consist with terminology used by Grafana project
-CHANGED: Modified netapp-manager to export dashboards to files in the same format as from the GUI, and import to add required
          elements back in.  Results is interchangeability between API and GUI exported / imported dashboards.
-FEATURE: Default grafana dashboards updated with multi template feature introduced in Grafana 2.1.1
-FIXED:   Updated netapp-manager Grafana dashboard management to be compatible with Grafana 2.1.1; due to Grafana API changes
          Grafana 2.0 is no longer compatible for exports
-CHANGED: Allow self-signed certificates and hostname mismatches in netapp-manager for upload/download of dashboards


1.2RC3 (5-Aug-2015)
-FIXED:   Fixed host_port to work correctly when specifying a port other than 443
-FIXED:   Optimization to call localtime less frequently in logging routine
-FIXED:   Optimization to reuse Graphite server network socket and reinitialize only upon failure
-FEATURE: Added logging message 'Startup complete' to inform user that the poller has completed startup and is entering the polling loop.
-FEATURE: Added support for OnCommand Unified Manager 6.3
-FIXED:   netapp-worker will error and exit if site includes whitespace characters
-FIXED:   For an unknown reason sometimes perl will error on running all plugins with something like
          "Global symbol "$obj" requires explicit package name" even though it is properly defined and worked for hundreds of iterations
          prior.  If a plugin fails we now restart the netapp-worker process to try and clear the error.

1.2RC2 (9-Jul-2015)
-FIXED:   Identification of release string parsing for OCUM works incorrectly for X/D/P releases
-CHANGED: Default netapp-harvest.conf file disables autosupport
-FEATURE: Added support for disabled pollers (host_enabled = 0) into netapp-manager so that it will not manage them and instead show
          them as DISABLED

1.2RC1 (26-Jun-2015)
-FEATURE: Initial RC release for posting to Toolchest
-FEATURE: Added test for hostname resolution in netapp-worker to ease troubleshooting of config mistakes
-CHANGED: Removed .pl extension from netapp-worker.pl and netapp-manager.pl.  If later these are rewritten in another language
          there will be no change for users
-CHANGED: Name of this file from README.txt to CHANGES.txt

1.2X20 (15-Jun-2015)
-CHANGED: All files that include '_' in the name have been updated to '-' to match most linux software packages
-CHANGED: netapp_worker.pl perl minimum reduced to 5.10.0 to meet standard version on SLES 11 SP3

1.2X19 (7-Jun-2015)
-CHANGED: Renamed netapp_harvest.pl to netapp_worker.pl, and manager.pl to netapp_manager.pl
-FEATURE: Added ability to upload and download Grafana 2.x dashboards using netapp_manager.pl  Configuration of 
          Grafana server is in new [grafana] section of the conf file.
-FEATURE: Added default Grafana 2.x dashboards in the ./grafana subdir.

1.2X18 (2-Jun-2015)
-FEATURE: Changed configuration technique from using many individual host files to using a single conf file.
          The manager.conf file is also no longer used and is replaced by the single conf file.  The netapp_harvest.pl
          and manager.pl files have been updated to work with the new format.
-FEATURE: Modified to send STDERR to logfile
-FIXED:   Uninitialized value warnings line 131, 133, in cdot-volume introduced in 1.2X17
-FEATURE: Created 7dot volume plugin to create total_data metric which is simply read_data + write_data
-FEATURE: Introduced host parameter 'latency_io_reqd'.  If the IOPs for some IOPs counter is less than the value
          specified then the corresponding latency counter will not be submitted.  This feature reduces outliers
          that cause instances to show up in the top resources views and is implemented for for volumes (cDOT and 7dot)
          and qos (cDOT). This feature supersedes the original feature implemented in 1.2X17.

1.2X17 (22-May-2015)
-FEATURE: Modified cDOT volume plugin and templates to do roll-up at aggr level (in addition to SVM and node)
-FEATURE: Modified cDOT volume plugin to skip sending read/write/other/avg latency if less than 1 IOPs to prevent misleading peaks at low IO load
-FEATURE: Modified cDOT volume plugin to create total_data metric which is simply read_data + write_data

1.2X16 (18-May-2015)
-FEATURE: Added support for default templates.  If no specific template is passed the version will be learned from the monitored host
          and the most appropriate default template will be used.
-FEATURE: Added support to manager.pl to operate on all files in host directory using the default template in case no manager.conf is found
-FEATURE: Added support to manager.pl for restart
-FEATURE: Added support for Graphite metrics via udp.  Allowed host connection graphite_port syntax can be like '2013' (2013 tcp), '2003/tcp' (2003 tcp), and '2003/udp (2003 udp)
-CHANGED: Modified plugins cdot_iscsi_lif, cdot_wafl_hya_sizer, and cdot_wafl_times to use host_version instead of dot_version
-FEATURE: Enhanced logging messages for config of OCUM clusters and graphite_root config
-FEATURE: Added 7-mode curr_conn_user_cnt and curr_sess_cnt to cifs_stats config to show current connected user and session count
-FEATURE: Added 7-mode scsi_partner_data and scsi_partner_ops to lun config to show partner IOs
-FEATURE: Added 7-mode scancompletions_from_server_rate, scanrequests_throttled_current, scanrequests_completed to vscan config to improve analysis capabilities
-FEATURE: Added 7-mode wv_fsinfo_blks_used to volume to allow IOPs/TB IOP density calculation
-FEATURE: Added 7-mode miss_normal_lev0 to ext_cache_obj counters

1.2X15 (30-Mar-2015)
-FEATURE: Added 'uptime' counter to system object of all default DOT templates.  This counter increments when the system is running and resets to 0 on boot.
-FEATURE: Added sample conf file for cDOT 8.1
-FEATURE: Added support for Data ONTAP 7.3 in harvest and sample conf file
-FEATURE: Added AWA (flash pool sizing) support into cDOT 8.2 and 8.3 templates and added associated plugin
-FIXED:   Modified nic_common to set units in cDOT 8.2 plugin so it will normalize instead of staying in bytes.  Also modified associated plugin.
-FIXED:   Modified 7-mode vstorage _data counter overrides to be in b_per_sec to match actual value as seen in live systems
-FIXED:   Updated overrides for token_manager and lun counters to improve on DOT metadata to display as a rate.
-CHANGED: Modified EMS log messages to be more detailed
-CHANGED: Updated Perl code syntax throughout to work with older version of Perl (tested with 5.10.1)
-CHANGED: Default cDOT template volume:node counters for non-protocol specific roll-up moved to volume object due to bug 899768

1.2X14 (20-Feb-2015)
-FEATURE: DOT 8.3 support for QoS Autovols.  QoS stats now submitted for all vols regardless if they are in a user-defined policy-group or not
-FIXED:   QoS instance collection is optimized (only learn instances for workload or workload_volume, and use these for the related detail object instance list)
-CHANGED: Moved conf file samples to samples subdir.  Copy the appropriate one into place for your configuration
-FIXED:   Require minimum Perl 5.14.1 due to used Perl syntax

1.2X13 (16-Feb-2015)
-FIXED:   DOT 8.3 default config now uses 'disk:constituent' instead of 'disk' to support disk partitioning
-FIXED:   7-mode default conf includes override for nfsv3_op_count and cifs_op_count to have it display correctly as a rate and not a delta
-CHANGED: Added plugin options to cdot-volume plugin to allow control of vol_summary at svm and node levels.  Default is svm is on, node is off.
-FEATURE: Added volume:node collection for cDOT.  Includes per protocol stats at the node level.  This is d-blade, the protocol counters are n-blade,
          so both are useful to admins depending on your perspective of frontend or backend traffic.
-CHANGED: Modified object types related to 7dot CIFS and related plugins to improve metrics hierarchy
		  
1.2X12 (28-Jan-2015)
-FIXED:   Updated LUN metric hierarchy placement in 7-mode using new plugin
-CHANGED: Changed overall hierarchy to be netapp.[perf|perf7|capacity|poller].[site] as graphite prefix
-CHANGED: Changed cDOT8.2 and 8.3 sample template to collect cifs_op_count instead of cifs_op_pct
-FEATURE: Added submission of node.iscsi and node.fcp counters to iscsi_lif and fcp_lif plugins
-FEATURE: Added iscsi_lif read_data and write_data for cDOT 8.3 (counter introduced in 8.3)

1.2X11 (17-Jan-2015)
-FIXED: LUN metric hierarchy placement in cDOT (regression from 1.2X10)
-FIXED: Qtree quota capacity is enabled by default in OCUM sample config files

1.2X10 (16-Jan-2015)
-FEATURE: Added support for workload and workload_detail QOS counters.  Submits per workload (file, LUN, Vol, SVM)
          and rolled-up per policy-group metrics.
-FEATURE: Added qtree quota capacity support for OCUM integration
-FEATURE: Added plugin_options to adjust their behaviour and avoid need to edit plugins directly
-FIXED:   lif collection on DOT 8.3 was broken due to changes in instance names and is now fixed

1.2X9 (18-Dec-2014)
-FEATURE: Changed graphite_leaf and graphite_root usage for OCUM configs to be more flexible

1.2X8 (17-Dec-2014)
-FIXED:   Bug that caused aggr capacity information to always be 0
-FEATURE: Added svm.vol_summary capacity counters that summarize capacity information at the SVM level

1.2X7 (13-Dec-2014)
-Updated manager.pl to allow execution from other locations; needed for init.d script to function properly
-Rename installation copy of manager.conf to manager.sample.conf.  This eases upgrades in the future as
 extracting a new installation image on top of an existing installation will not overwrite locally modified files.
-Beginning in 1.2X6, the default host 'normalized_xfer' option was changed from kb_per_sec to mb_per_sec.  For
 an upgrade you may prefer to retain the kb_per_sec, but for new installations most people prefer to 
 see metrics natively in MB/s.  All values are of type float, so the precision to a more granular unit can
 always be done at time of display if needed. 

1.2X6 (12-Dec-2014)
-Works with cDOT 8.3, but NOT yet tested with disk partitioning
-Adjusted sleep time on initial startup so that per object data polls will always begin at a time that matches
 the poll frequency of the graphite buckets; i.e. if you poll some counter every 300 seconds we must start on
 some minute that is evenly divisible by 5 to prevent two polls from landing in the same graphite bucket
-Added meta-metric pluginTime to measure seconds that the object plugin took to execute
-Changed 'disk' to 'aggr' in graphite hierarchy; namespace will change as a result.  Existing installs should
 rename the 'disk' folder in the hierarchy to 'aggr' to preserve history.
-New manager script to start/stop/check status of pollers 'manager.pl' and associated configuration file
 manager.conf
-Support of SSL client certificates for authentication (in addition to the username/password) to provide
 a solution that avoids plaintext passwords in config files.  Review the README.txt in the cert/ subdir
 for setup instructions.
-Implemented full 2D array counter support (i.e. counters that have 2 x label-info).  Previously for 2D
 arrays a column was summed essentially making it a 1D array. As a result of this change any previously
 collected 2D array counters will need to be deleted from graphite hierarchy to avoid a collision.  An
 example would be wafl_wafl_time_ms where prior to the fix there would be one counter for each
 column (i.e. STRIPE, SERIAL, etc) that included a sum of all message types, and and after the fix there
 will be a folder (i.e. STRIPE, SERIAL, etc) and then a counter file for each more detailed message
 (i.e. SPINNP_WRITE, SERIAL.SPINNP_READDIR, etc)
-Poller template variables related to polling frequency are now optional and will use the following
 defaults unless otherwise specified:
 'data_update_freq' = 60 (Poll for data every minute)
 'counter_update_freq' = 86400 (Poll for counter metadata 24 hr)
 'instance_update_freq' = 3600 (Poll for new instances every 60 min)
-Counter objects instance list refresh times are now skewed by 60 seconds each to stagger them and avoid
 intervals with many counter refreshes [that might cause skipped data polls].
-Added status logging message every 4hrs to logfile and Event Management System (EMS) of Data ONTAP system.
 Message includes the preceding time window in seconds, and then in that sample window the elapsed api and
 plugin time, num metrics submitted, and num skipped data polls. After each status message the values are reset.
-Added custom unit 'wafl_block'.  Use template override with a unit of wafl_block to normalize counters which are 
 natively in blocks (and usually with a unit of raw) to the 'normalized_xfer' unit from your host config file.
-Metrics are submitted with the client timestamp and not the storage timestamp to ensure alignment with 
 Graphite buckets and avoid seemingly missed datapoints [that were really two updates in one bucket].
-Modified eth_port plugin to add counters for percent utilization
-Renamed default cDOT config file counter group fcp-lif to fcp_lif, and iscsi-lif to iscsi_lif, nfs to nfsv3, 
 to be consistent with other metrics
-Added logfile autoflushing
-Added plugin for iscsi_lif to submit svm.iscsi summarized metrics.
-Modified plugin for fcp_lif to submit svm.fcp summarized metrics and node.fcp_port metrics.
-Added plugin for volume to submit svm.vol, svm.vol_summary, node.vol_summary.  Existing implementations will
 need to rename node.vol to node.vol_summary.
-Updated instance logic for fcp_lifs to use net-interface-get-iter instead of perf APIs to avoid bug 764178 
-Added capacity monitoring using APIs to collect capacity information of aggr/vol/luns from an OnCommand 6.x server
-Updated to SDK 5.3
-Added ./util directory with two utilities useful for browsing counter manager metrics and a sample init.d startup
 script.  For utility scripts run with no arguments to see usage.
-Helpful rename commands to cope with the changes just stop the poller, cd to the root of your whisper storage
 files for the harvest hierarchy, and run the following: 
 find . -name disk -exec bash -c 'mv "$0" "${0/disk/aggr}"' {} \;
 find . -name fcp-lif -exec bash -c 'mv "$0" "${0/fcp-lif/fcp_lif}"' {} \;
 find . -name iscsi-lif -exec bash -c 'mv "$0" "${0/iscsi-lif/iscsi_lif}"' {} \;
 find . -name nfs -exec bash -c 'mv "$0" "${0/nfs/nfsv3}"' {} \;
 find . -name wafl_wafl_time_ms -exec rm -rf  {} \;
 cd within the node subdir of each monitored cluster
 find . -name vol -exec bash -c 'mv "$0" "${0/vol/vol_summary}"' {} \; 


1.1 (10-Oct-2014)
-Updated instance names in metric path to replace '.' with '_'; namespace might change as a result
 (i.e. metric like /vol/data.lun used to be /vol/data/lun/read_data and would now be data_lun/read_data).
 fcp_lif plugin was updated to be compatible with the change as well
-Updated instance logic for LUNs to use lun-get-iter instead of perf APIs to avoid bugs 764178 and 789091
-Updated pathing for LUNs to be vol.<volname>.lun.<lun_name> for easier use with wildcards
-Add dynamic batching support for data counters to reduce number of instances requested if 
 resource limited exceeded errors are seen.
 

1.0 (02-Sep-2014)
-Initial external version release