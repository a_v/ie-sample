#!/usr/bin/perl
#==================================================================#
#                                                                  #
# NetApp Harvest Manager                                           #
#                                                                  #
# Manage many netapp-worker poller instances and import/export     #
# Grafana dashboards.                                              #
#                                                                  #
# Author: Chris Madden, NetApp                                     #
#                                                                  #
# Copyright (c) 2015 NetApp, Inc. All rights reserved.             #
# Specifications subject to change without notice.                 #
#                                                                  #
#==================================================================#

my $VERSION = '1.3';

use strict;
use warnings;
use Getopt::Long;
use FindBin qw($Bin $Script);
use JSON;
use LWP;
use IO::Socket;
use IO::Socket::SSL;

my %options;

sub print_usage();
sub update_ps();

GetOptions( \%options, "start", "stop", "restart", "status", "confdir=s", "logdir=s", "conf=s", "poller=s", "group=s", "import", "export", "h|help|?", "v");

# Check options passed
{
	my $found = 0;
	for (qw (status start stop restart import export) )
	{
		$found++ if (exists $options{$_});
	}
	
	if (($found != 1) || (exists $options{h}))
	{
		print_usage();
		exit;
	}
}

my %conf;
my %daemon;

# Set defaults for CLI args
$options{conf}    = "netapp-harvest.conf" unless (exists $options{conf});
$options{confdir} = $Bin unless (exists $options{confdir});
$options{logdir}  = $Bin."/log" unless (exists $options{logdir});

# Read in conf file
{
	my $section;
	open (FILE, "<$options{confdir}/$options{conf}") || die "[ERROR] Cannot read config file [$options{confdir}/$options{conf}]: [$!]\n";

	while (<FILE>)
	{
		my $line = $_;
		chomp ($line);
		
		my ($header, $key, $value);
		
		# Discard lines that begin with whitespace and a # symbol, a # symbol, are only whitespace characters, or are blank
		next if ($line =~ /^\s*#/);
		next if ($line =~ /^\s+$/);
		next if ($line eq '');
		
		## We should have only valid config lines now...
		
		# Is section header?
		if ( ($header) = $line =~ /\[(.*)\]/)
		{
			$header =~ s/^\s+|\s+$//g;
			$section = $header;
			print "[OK     ] Line [$.] is Section [$section]\n" if (exists $options{v});
		}
		# Is key/value pair?
		elsif ( ($key, $value) = $line =~ /(.*?)=(.*)/)
		{
			$key =~ s/^\s+|\s+$//g;
			$value =~ s/^\s+|\s+$//g;
			# Handle re-writing depreciated site -> group
			if ($key eq 'site')
			{
				print "[OK     ] [$section] key [site] is depreciated; aliasing to [group]. Rename [site] to [group] to remove this message.\n" if (exists $options{v});
				$key = 'group';
			}
			$conf{$section}{$key} = $value;
			$value = '**********' if ( ($key eq 'password') || ($key eq 'grafana_api_key') );
			print "[OK     ] Line [$.] in Section [$section] has Key/Value pair [$key]=[$value]\n" if (exists $options{v});
		}
		# Warn if we got something else...
		else
		{
			print "[WARNING] Line [$.] in Section [$section] cannot be parsed as Header or Key/Value pair: [$line]\n";
		}
	}
	close (FILE);
}

## Handle dashboard imports and exports
if ((exists $options{import}) || (exists $options{export}) )
{
	# Verify config elements exist
	if (! exists $conf{global})
	{
		print "[ERROR  ] No [grafana] section found\n";
		exit -1;
	}
	elsif (! exists $conf{global}{grafana_api_key})
	{
		print "[ERROR  ] In [grafana] section no grafana_api_key variable found\n";
		exit -1;
	}
	elsif (! exists $conf{global}{grafana_url})
	{
		print "[ERROR  ] In [grafana] section no grafana_url variable found\n";
		exit -1;
	}

	print "[OK     ] Will import dashboards to [$conf{global}{grafana_url}]\n" if (exists $options{import});
	print "[OK     ] Will export dashboards from [$conf{global}{grafana_url}]\n" if (exists $options{export});
	print "[OK     ] Dashboard directory is [$Bin/grafana]\n";

	# Do a quick test to make sure the grafana server socket is open because if dest host is
	# behind a firewall LWP will hang forever

	# Strip trailing slash if there is one because users like to put it there and it is unwanted
	$conf{global}{grafana_url} =~ s/\/$//g;

	# Get addr and port (regexp is from https://www.ietf.org/rfc/rfc2396.txt)
	my ($addr, $port);
	if ($conf{global}{grafana_url} =~ m!^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?!)
	{
		$addr = $4;
		$port = 80 if ($2 eq 'http');
		$port = 443 if ($2 eq 'https');
		if ($addr =~ /(.*):(\d{1,5})/)
		{
			($addr, $port) = ($1, $2);
		}
	}

	my $sock = new IO::Socket::INET ( Timeout => 5, PeerAddr => $addr, PeerPort => $port, Proto => 'TCP' );
	
	if (! $sock)
	{
		if ( my $ip_packed = gethostbyname($addr) )
		{	
			my ($a,$b,$c,$d) = unpack('C4',$ip_packed);
			print "[ERROR  ] Unable to establish TCP connection to $addr:$port [$a.$b.$c.$d:$port]\n";
		}
		else
		{
			print "[ERROR  ] Unable to resolve name $addr\n";
		}
		print "[ERROR  ] Exiting due to fatal error.\n";
		exit (-1);
	}
	else
	{
		close($sock);
	}
	
	# Setup basic UserAgent details needed for import/export, and ignore SSL cert issues since many use self signed ones...
	my $ua = LWP::UserAgent->new( ssl_opts => {verify_hostname => 0, SSL_verify_mode => SSL_VERIFY_NONE}, timeout => 10);
	$ua->default_header( 
		'Accept' => 'application/json',
		'Content-Type' => 'application/json;charset=UTF-8',
		'Authorization' => "Bearer ".$conf{global}{grafana_api_key} );

	# Logic for exporting dashboards
	if (exists $options{'export'})
	{
		mkdir $Bin."/grafana" unless (-d $Bin."/grafana/");	
				
		# Search dashboard list
		my $response = $ua->get($conf{global}{grafana_url}.'/api/search');
		if (! $response->is_success)
		{
			print "[ERROR  ] Failed to export dashboard list due to reason: ", $response->status_line(), "\n";
			exit -1;
		}
		
		# Parse export list and find dashboards that include our desired tag
		my @uri_list;
		my $dashboard_list = decode_json($response->content());
		for my $dash ( @{ $dashboard_list})
		{
			print "[OK     ] -Found dashboard [$dash->{'uri'}]\n" if $options{v};

			# If no tag exists or it is blank
			if ( (! exists $conf{global}{grafana_dl_tag}) || ($conf{global}{grafana_dl_tag} eq '') )
			{
				push @uri_list, $dash->{'uri'}; 
			}
			# Add only if slug has matching tag
			else
			{
				for my $tag (@{ $dash->{'tags'}})
				{
					if ($tag eq $conf{global}{grafana_dl_tag})
					{
						push @uri_list, $dash->{'uri'}; 
						last;
					}
				}
			}
		}
	
		# export each slug
		for my $uri (@uri_list)
		{
			my $response = $ua->get($conf{global}{grafana_url}.'/api/dashboards/'.$uri);
	
			if ($response->is_success)
			{
				# Get response text to parsed JSON
				my $json = from_json($response->content());
								
				# Remove any query template items [they should be rediscovered]
				my $num = 0;
				for my $item ( @{$json->{dashboard}->{templating}->{list}})
				{
					if ($item->{type} eq 'query')
					{
						delete $json->{dashboard}->{templating}->{list}->[$num]->{options};
						delete $json->{dashboard}->{templating}->{list}->[$num]->{current};
					}
					$num++
				}
				# Remove outer dashboard element wrapper
				$json = $json->{dashboard};
				
				my $json_pretty = to_json($json, {utf8 => 1, pretty => 1});
				my $short_filename = "$uri.json";
				$short_filename =~ s/\//_/g;
				my $filename = $Bin."/grafana/".$short_filename;
				if ( open (FILE, ">$filename") )
				{
					binmode FILE; # Set linux carriage returns
					print FILE $json_pretty;
					close FILE;
					print "[OK     ] Exported [$uri] to dashboard file [$short_filename]\n";
				}
				else
				{
					print "[ERROR  ] Exported [$uri] but failed to create file [$filename] due to reason: $!\n";
				}
			}
			else
			{
				print "[ERROR  ] Failed to export dashboard [$uri] due to reason: ", $response->status_line(), "\n";
			}
		}
	}
	# Logic for importing dashboards
	elsif (exists $options{'import'})
	{
		# Get all dashboards
		my @import_files;
		opendir (DIR, "$Bin/grafana") || die "[ERROR  ] Cannot open $Bin/grafana due to reason: $!";
		
		while (my $file = readdir(DIR))
		{

			#next unless ($file =~ /\.json$/); # Make sure a .json file
	
			my $filename = $Bin."/grafana/".$file;
			next unless (-f $filename);
			open (FILE, "<$filename") || die "[ERROR  ] Cannot open file $file due to reason: $!\n";
			my $filedata = do { local $/; <FILE> };
			close FILE;
			
			# Parse JSON in an eval so we can skip warn on invalid syntax and skip
			my $json;
			eval
			{
				$json = from_json($filedata);
			};
			if ($@)
			{
				print "[WARNING] Cannot import dashboard file [$file] because it is not valid JSON\n";
				print "[WARNING] -Error was :$@\n" if $options{v};
				next;
			}
			
			# Set id to null
			$json->{id} = JSON::null; 			
			
			#Build new json which is compatible with the API forcing overwrite and adding the dashboard element
			my $new_json = JSON::to_json( { dashboard => $json, overwrite => JSON::true } );
			
			# Build request
			my $req = HTTP::Request->new(POST => $conf{global}{grafana_url}.'/api/dashboards/db/');
			$req->content($new_json); 
			my $response = $ua->request($req);

			my $resp_success = $response->is_success();

			# If status code was OK also check if JSON exists and reports success
			my $resp_json;
			if ($resp_success)
			{
				eval
				{
					$resp_json = from_json($response->content());
				};

				if ($@)
				{
					$resp_success = 0;
				}
				else
				{
					$resp_success = 0 if ($resp_json->{status} ne 'success');
				}
			}

			if (! $resp_success)
			{
				print "[ERROR  ] Failed to import dashboard [$file] due to error: ", $response->status_line(), "\n";
				print "[ERROR  ] -Response was :".$response->content(), "\n";
				print "[ERROR  ] Exiting due to fatal error.\n";
				exit -1;
			}
			
			print "[OK     ] Imported dashboard [$file] successfully\n";
			print "[OK     ] -Response was :".$response->content(), "\n" if $options{v};
		}
		closedir (DIR);
	}
	exit 0;
}
## If we get here we want to manage workers...
else
{
	my $exit_code = 0; # Assume good!
	if ($^O eq 'MSWin32')
	{
		print "[ERROR  ] Worker management (status/stop/start/restart) not supported on Windows\n";
		exit (-1);
	}

	# Evaluate / update / delete entries
	for my $p (keys %conf)
	{
		# No work to do for default or global section
		next if ($p eq 'default');
		next if ($p eq 'global');
		
		# Copy in defaults to section
		for my $k (keys %{$conf{default}})
		{
			$conf{$p}{$k} = $conf{default}{$k} if (! exists ($conf{$p}{$k}));
		}
		
		# Detect pollers that are missing group
		if (! exists $conf{$p}{group})
		{
			print "[ERROR  ] Poller [$p] group field is missing; unable to manage this poller\n";
			delete $conf{$p};
			next;
		}

		# Detect pollers that have group with whitespace in it
		if ($conf{$p}{group}  =~ /\s/)
		{
			print "[ERROR  ] Poller [$p] group field has whitespaces in it; unable to manage this poller\n";
			delete $conf{$p};
			next;
		}
		
		# Remove matches based on poller name
		if ( exists $options{poller} )
		{
			delete $conf{$p} unless ( $p =~ /$options{poller}/);
		}	
		
		# Remove matches based on group name
		if ( exists $options{group} )
		{
			delete $conf{$p} unless ( $conf{$p}{group} =~ /$options{group}/);
		}	
	}
	
	# Remove default and grafana entries as they are not pollers
	delete $conf{default};
	delete $conf{global};
	
	printf ("%-15s %-20s %-20s\n", "STATUS", "POLLER", "GROUP");
	printf ("%-15s %-20s %-20s\n", "###############", "####################", "##################");
	
	## Handle status
	if ($options{status})
	{
		update_ps();
		my $exit = 0;
		
		for my $p (sort keys %conf)
		{
			if ((exists $conf{$p}{host_enabled}) && ($conf{$p}{host_enabled} == 0))
			{
				printf ("%-15s %-20s %-20s\n", "[DISABLED]", $p, $conf{$p}{group});
			}
			elsif (exists $daemon{$p})
			{
				printf ("%-15s %-20s %-20s\n", "[RUNNING]", $p, $conf{$p}{group});
			}
			else
			{
				printf ("%-15s %-20s %-20s\n", "[NOT RUNNING]", $p, $conf{$p}{group});
				$exit_code = 3;
			}
		}
	}
	
	## Handle stop
	if ($options{stop} || $options{restart})
	{
		update_ps();
		
		for my $p (sort keys %conf)
		{
			if ((exists $conf{$p}{host_enabled}) && ($conf{$p}{host_enabled} == 0))
			{
				printf ("%-15s %-20s %-20s\n", "[DISABLED]", $p, $conf{$p}{group});
			}
			elsif (exists $daemon{$p})
			{
				printf ("%-15s %-20s %-20s\n", "[STOPPED]", $p, $conf{$p}{group});
				`kill $daemon{$p}`;
			}
			else
			{
				printf ("%-15s %-20s %-20s\n", "[NOT RUNNING]", $p, $conf{$p}{group});
			}
		}
	}
	
	## Handle start
	if ($options{start} || $options{restart})
	{
		update_ps();
		
		for my $p (sort keys %conf)
		{
			if ((exists $conf{$p}{host_enabled}) && ($conf{$p}{host_enabled} == 0))
			{
				printf ("%-15s %-20s %-20s\n", "[DISABLED]", $p, $conf{$p}{group});
			}
			elsif (exists $daemon{$p})
			{
				printf ("%-15s %-20s %-20s\n", "[RUNNING]", $p, $conf{$p}{group});
			}
			else
			{
				printf ("%-15s %-20s %-20s\n", "[STARTED]", $p, $conf{$p}{group});
				my $cmd = "$Bin/netapp-worker -poller $p -conf $options{conf} -confdir $options{confdir} -logdir $options{logdir} -daemon";
				$cmd .= " -v" if (exists $options{v});
				`$cmd`;
			}
		}
	}
	
	exit $exit_code;
}

sub update_ps()
{
	my $cmd = 'ps -ef';
	undef %daemon;
	for (`$cmd`)
	{
		#root      1825     1  4 15:03 ?        00:00:04 /usr/bin/perl ./netapp-worker -poller blob1 -daemon
		#root      1826     1  4 15:03 ?        00:00:04 /usr/bin/perl ./netapp-worker -poller blob2 -conf test.conf -daemon
		my ($pid, $poller) = /\S+\s+(\d+).*netapp-worker.*\-poller (.*?) \-conf $options{conf}/;
		$daemon{$poller} = $pid || next;
	}
}

sub print_usage()
{
print <<EOF;

Usage: $Bin/$Script {-status|-start|-stop|-restart|-import|-export} [-poller <str>] [-group <str>] [-conf <str>] [-confdir <str>] [-logdir <str>] [-h] [-v]

 PURPOSE:
  Stop/start/restart/status of netapp-worker poller processes and import/export Grafana dashboards
 VERSION: 
  $VERSION
 ARGUMENTS:
  Required (one of):
    -status            Show status of all matching pollers
    -start             Start all matching pollers
    -stop              Stop all matching pollers
    -restart           Stop/start all matching pollers
    -import            Import dashboard json files to Grafana server
    -export            Export dashboard json files from Grafana server
  Optional:
    -poller <str>      Filter on poller names that match regex <str> (only valid for status/start/stop/restart)
    -group <str>       Filter on group names that match regex <str> (only valid for status/start/stop/restart)
    -conf <str>        Name of config file to use to find poller name
                       (default: netapp-harvest.conf)
    -confdir <str>     Name of directory where config file is located
                       (default: $Bin)
    -logdir <str>      Name of directory where log files should be written
                       (default: $Bin/log)
    -h                 Show this help
    -v                 Show verbose output, and if starting pollers, start them with verbose logging
 EXAMPLES:
  Check status of all pollers
   $0 -status
  Start all pollers that are not already running
   $0 -start
  Stop all pollers with poller name that includes netapp
   $0 -stop -poller netapp
  Restart all pollers with poller name that includes netapp at group AMS
   $0 -restart -poller netapp -group AMS
  Import all dashboards from the grafana subdirectory to Grafana
   $0 -import
   
EOF
}
