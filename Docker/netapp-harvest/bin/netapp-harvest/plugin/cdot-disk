#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         disk
# Graphite Leaf:  'node.{node_name}.aggr.{raid_name}.{raid_type}.{disk_speed}'    
# Counters req'd: disk_speed, disk_busy
#
# Plugin Options:  'disk' => [0|1] (enables/disables metrics per disk; default is disabled)
#                  'rg'   => [0|1] (enables/disables metrics per raidgroup; default is enabled)
#                  'plex' => [0|1] (enables/disables metrics per plex; default is enabled)
#                  'aggr' => [0|1] (enables/disables metrics per aggr; default is enabled)
#                  'node' => [0|1] (enables/disables metrics per node; default is enabled)
#
#                   Typically tracking at raidgroup level and less granular is sufficient and
#                   avoids diskspace and IO requirements if per disk is enabled
#
# Description: Calcs max value of HDD at each layer in the 
# node/aggr/plex hierarchy, and HDD or SDD at rg layer.  Also
# rewrites the metric path to be more user friendly.
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2015 NetApp, Inc. All rights reserved.             
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================
use strict;
use warnings;
use List::Util qw( max sum);

# Declare our globals
our %connection;
our %poller;
our %counter;
our %instance;
our %data;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");

my %h; # Hold hash of disk level values
my %raidgroups; # Hold hash of raidgroup speeds
my %hybrid;     # Hold hash of hybrid aggr/rg
my %max_node;   # Hold hash of max per node
my %max_aggr;   # Hold hash of max per aggr
my %max_plex;   # Hold hash of max per plex
my $timestamp_for_summmarized;

# Manage plugin options setting defaults
my %emit_level =   (	disk => '0',
                        rg => '1',
						plex => '1',
						aggr => '1',
						node => '1');

# Allow tweaking in poller config
for (keys %{$poller{$obj}{plugin_options}})
{
	$emit_level{$_} = $poller{$obj}{plugin_options}{$_};
}
						
my @n_emit_items;

# Rewrite metrics:
# INCOMING: dev.nltestlab.cmdemo.node.cmdemo-03.aggr./aggr1_cmdemo_03_sas_15k/plex0/rg1/0b.12.14.data.15000.user_reads 5 1405234861
# OUTGOING:  dev.nltestlab.cmdemo.node.cmdemo-01.aggr.aggr1_cmdemo_01_fp_sas_15k.plex0.rg0.data-1a-11-5.disk_busy 0.240825688073394 1405242541
foreach my $item (@emit_items)
{
	#logger ("DEBUG", "Incoming to plugin:   $item");
	my ($start, $aggr, $plex, $rg, $disk, $type, $speed, $param, $val, $timestamp) = $item =~ /(.*)\.aggr\.\/(.*)\/(plex\d*)\/(rg\d*)\/(.*)\.(data|parity|dparity|tparity)\.(.*)\.(.*) (.*) (\d+)/;
	next unless (defined $start);
	#logger ("DEBUG", "Parsed speed is $speed");
	
	#is = /n01fc01/plex0/rg0/3a.51.data.user_reads
	#to = n01fc01.plex0.rg0.3a-51-data.user_reads
	$disk =~ s/\./-/g;
	$disk = "$type-$disk";
	
	my $n_item = "$start.aggr.$aggr.$plex.$rg.$disk.$param $val $timestamp";
	
	# Create metric at individual disk level
	push @n_emit_items, $n_item  if ($emit_level{disk});
	
	# Add value to array for later use in determining max
	push @{$h{$start}{$aggr}{$plex}{$rg}{$param}}, $val;

	# Save speed of aggr/rg
	$raidgroups{$aggr}{$rg} = $speed;
	
	# Grab a single timestamp from the poll and use it for max metrics
	$timestamp_for_summmarized = $timestamp;
}

# Find hybrid aggrs/rg
for my $aggr (keys %raidgroups)
{
	my $last_rg_type = '';
	for my $rg (keys %{$raidgroups{$aggr}})
	{
		if ($last_rg_type eq '')
		{
			$last_rg_type = $raidgroups{$aggr}{$rg};
		}
		elsif ($raidgroups{$aggr}{$rg} ne $last_rg_type)
		{
			$hybrid{$aggr} = 1;
		}
	}
}


## Step through hash, determine max for the node/aggr/plex/rg for each param, and set that max at each level
for my $node (keys %h)
{
	# At aggr level
	for my $aggr (keys %{$h{$node}})
	{
		# At plex level
		for my $plex (keys %{$h{$node}{$aggr}})
		{
			# At rg level
			for my $rg (keys %{$h{$node}{$aggr}{$plex}})
			{
				# At param level
				for my $param (keys %{$h{$node}{$aggr}{$plex}{$rg}})
				{
					my $max = max @{$h{$node}{$aggr}{$plex}{$rg}{$param}};
					
					# Submit metric for raidgroup level max
					push @n_emit_items, "$node.aggr.$aggr.$plex.$rg.$param $max $timestamp_for_summmarized" if ($emit_level{rg});
					
					# Don't incl ssd metrics in node/aggr/plex levels if hybrid aggr and disk is in ssd rg
					next if (exists $hybrid{$aggr} && $raidgroups{$aggr}{$rg} eq 'N/A');
					
					# Set metrics for node, aggr, plex level max
					$max_node{$node}{$param} = $max if (!exists $max_node{$node}{$param} || $max_node{$node}{$param} < $max);
					$max_aggr{$node}{$aggr}{$param} = $max if (!exists $max_aggr{$node}{$aggr}{$param} || $max_aggr{$node}{$aggr}{$param} < $max);
					$max_plex{$node}{$aggr}{$plex}{$param} = $max if (!exists $max_plex{$node}{$aggr}{$plex}{$param} || $max_plex{$node}{$aggr}{$plex}{$param} < $max);
				}
			}
		}
	}
}
# Emit metrics for each node
for my $node (keys %max_node)
{
	for my $param (keys %{$max_node{$node}})
	{
		push @n_emit_items, "$node.aggr.$param $max_node{$node}{$param} $timestamp_for_summmarized" if ($emit_level{node});
	}
}

# Emit metrics for each node/aggr
for my $node (keys %max_aggr)
{

	for my $aggr (keys %{$max_aggr{$node}})
	{
		for my $param (keys %{$max_aggr{$node}{$aggr}})
		{
			push @n_emit_items, "$node.aggr.$aggr.$param $max_aggr{$node}{$aggr}{$param} $timestamp_for_summmarized" if ($emit_level{aggr});
		}
	}
}


# Emit metrics for each node/aggr/plex
for my $node (keys %max_plex)
{
	for my $aggr (keys %{$max_plex{$node}})
	{
		for my $plex (keys %{$max_plex{$node}{$aggr}})
		{
			for my $param (keys %{$max_plex{$node}{$aggr}{$plex}})
			{
				push @n_emit_items, "$node.aggr.$aggr.$plex.$param $max_plex{$node}{$aggr}{$plex}{$param} $timestamp_for_summmarized" if ($emit_level{plex});
			}
		}
	}
}

@emit_items = @n_emit_items;
return 0;
