#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         fcvi
# Graphite Leaf: 'node.{node_name}.fcvi.{instance_name}',
# Counters req'd: N/A, but only supports roll-up for
#                 rdma_write_throughput rdma_write_ops rdma_write_avg_latency
#
#
# Description: Create fcvi node-level roll-ups
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2015 NetApp, Inc. All rights reserved.             
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================

use strict;
use warnings;

# Declare our globals
our %poller;
our %connection;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");

my @n_emit_items;
my %phys_h;
my %virt_h;

#List of counters eligible for roll-up to node and SVM
my %supported_rollup_metric_list = map { $_ => 1 } qw(rdma_write_throughput rdma_write_ops rdma_write_avg_latency);

# Rewrite metrics:

# IN  netapp.perf.nl.cluster2.node.node5.fcvi.fcvi_device_0.rdma_write_avg_latency 33443 1123123
#     start---------------------------------> device------> counter--------------> val-> ts------->

my ($timestamp);
push @n_emit_items, @emit_items;

foreach my $item (@emit_items)
{
	#logger ("DEBUG", "[$obj] IN: $item");
	my ($start, $device, $counter, $value, $ts) = $item =~ /(.*)\.fcvi\.(.*?)\.(.*?) (.*) (\d+)/;
	#next unless defined $start; #Only process on matches
	
	$timestamp = $ts;

	$phys_h{$start}{$device}{$counter} += $value;
}

foreach my $node (keys %phys_h)
{
	my %tmp_h_node;
	foreach my $device (keys %{$phys_h{$node}})
	{
		foreach my $counter (keys %{$phys_h{$node}{$device}})
		{
			# Can only roll-up counters that we know how to...
			next unless (exists $supported_rollup_metric_list{$counter});
			
			# Calc summary data
			if ($counter eq 'rdma_write_avg_latency') # So we can do weighted averages
			{
				$tmp_h_node{$counter} += ($phys_h{$node}{$device}{$counter} * $phys_h{$node}{$device}{rdma_write_ops});
			}
			else # Just sum them up
			{
				$tmp_h_node{$counter} += $phys_h{$node}{$device}{$counter};
			}
		}
	}

	#Finish math
	$tmp_h_node{rdma_write_avg_latency} = $tmp_h_node{rdma_write_avg_latency}  / $tmp_h_node{rdma_write_ops}  if ( (exists $tmp_h_node{rdma_write_ops})  && (exists $tmp_h_node{rdma_write_avg_latency}) );
	
	# Submit aggr counters
	foreach my $counter (keys %tmp_h_node)
	{
		push @n_emit_items, "$node.fcvi.$counter $tmp_h_node{$counter} $timestamp";
	}
		
}
	
@emit_items = @n_emit_items;
return 0;
