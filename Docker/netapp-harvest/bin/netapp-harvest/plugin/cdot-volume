#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         volume
# Graphite Leaf: 'svm.{vserver_name}.{node_name}.{parent_aggr}.{instance_name}'
# Counters req'd: N/A, but only supports vol_summary roll-up for
#                 read_data write_data read_ops write_ops other_ops total_ops
#                 read_latency write_latency other_latency avg_latency
#
# Plugin Options:  'svm'  => [0|1] (enables/disables metrics per svm; default is enabled)
#                  'node' => [0|1] (enables/disables metrics per node; default is enabled)
#                  'aggr' => [0|1] (enables/disables metrics per node; default is enabled)
#
# Description: Create svm.vol_summary and node.vol_summary metrics.
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2016 NetApp, Inc. All rights reserved.
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================


use strict;
use warnings;

# Declare our globals
our %poller;
our %connection;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");

# Manage plugin options setting defaults
my %emit_level =   (	svm  => '1',
                        node => '1',
						aggr => '1');

# Allow tweaking in poller config
for (keys %{$poller{$obj}{plugin_options}})
{
	$emit_level{$_} = $poller{$obj}{plugin_options}{$_};
}
		
my @n_emit_items;
my %phys_h;
my %virt_h;

#List of counters eligible for roll-up to node and SVM
my %supported_rollup_metric_list = map { $_ => 1 } qw(read_data write_data total_data read_ops write_ops other_ops total_ops read_latency write_latency other_latency avg_latency);

# Rewrite metrics:

# IN: dev.sdt-cdot1.svm.sdt-vs-nas1.sdt-cdot1-01.aggr01.vol_sio.read_data 4489.333336 1417589800
#     start------->     svmname---> node-------> aggr-> vol---> counter-> val-------> ts------->

my ($prefix, $timestamp);

foreach my $item (@emit_items)
{
	#logger ("DEBUG", "[$obj] IN: $item");
	my ($start, $svm, $node, $aggr, $vol, $counter, $value, $ts) = $item =~ /(.*)\.svm\.(.*?)\.(.*?)\.(.*?)\.(.*?)\.(.*?) (.*) (\d+)/;
	#next unless defined $start; #Only process on matches
	
	# Setup stuff for per node, aggr, and SVM counters
	$prefix = $start;
	$timestamp = $ts;
	
	# Create total_data metric
	if (($counter eq 'read_data') || ($counter eq 'write_data'))
	{
		$phys_h{$node}{$aggr}{$vol}{'total_data'} += $value;
		$virt_h{$svm}{$vol}{'total_data'} += $value;
	}
	
	$phys_h{$node}{$aggr}{$vol}{$counter} += $value;
	$virt_h{$svm}{$vol}{$counter} = $value;
}




# Create individual vol and optionally SVM vol_summary metrics
foreach my $svm (keys %virt_h)
{
	my %tmp_h;
	
	foreach my $vol (keys %{$virt_h{$svm}})
	{
		foreach my $counter (keys %{$virt_h{$svm}{$vol}})
		{
			# Add individual vol metrics
			push @n_emit_items, "$prefix.svm.$svm.vol.$vol.$counter $virt_h{$svm}{$vol}{$counter} $timestamp";
			
			# Skip the rest of the processing if we don't want SVM level stats
			next unless ($emit_level{svm});
			
			# Can only roll-up counters that we know how to...
			next unless (exists $supported_rollup_metric_list{$counter});
			
			# Calc SVM summary data
			if ($counter eq 'read_latency') # So we can do weighted averages
			{
				$tmp_h{$counter} += ($virt_h{$svm}{$vol}{$counter} * $virt_h{$svm}{$vol}{read_ops});
			}
			elsif ($counter eq 'write_latency') # So we can do weighted averages
			{
				$tmp_h{$counter} += ($virt_h{$svm}{$vol}{$counter} * $virt_h{$svm}{$vol}{write_ops});
			}
			elsif ($counter eq 'other_latency') # So we can do weighted averages
			{
				$tmp_h{$counter} += ($virt_h{$svm}{$vol}{$counter} * $virt_h{$svm}{$vol}{other_ops});
			}
			elsif ($counter eq 'avg_latency') # So we can do weighted averages
			{
				$tmp_h{$counter} += ($virt_h{$svm}{$vol}{$counter} * $virt_h{$svm}{$vol}{total_ops});
			}
			else # Just sum them up
			{
				$tmp_h{$counter} += $virt_h{$svm}{$vol}{$counter};
			}
		}
	}
	
	# Add per SVM counters
	if ($emit_level{svm})
	{
		#Finish math for weighted averages
		$tmp_h{read_latency}  = $tmp_h{read_latency} / $tmp_h{read_ops}   if ( (exists $tmp_h{read_ops})  && (exists $tmp_h{read_latency}) );
		$tmp_h{write_latency} = $tmp_h{write_latency} / $tmp_h{write_ops} if ( (exists $tmp_h{write_ops}) && (exists $tmp_h{write_latency}) );
		$tmp_h{other_latency} = $tmp_h{other_latency} / $tmp_h{other_ops} if ( (exists $tmp_h{other_ops}) && (exists $tmp_h{other_latency}) );
		$tmp_h{avg_latency}   = $tmp_h{avg_latency} / $tmp_h{total_ops}   if ( (exists $tmp_h{total_ops}) && (exists $tmp_h{avg_latency}) );

		foreach my $counter (keys %tmp_h)
		{
			push @n_emit_items, "$prefix.svm.$svm.vol_summary.$counter $tmp_h{$counter} $timestamp";
		}
	}
}

# Create node and aggr vol_summary metrics if requested
if ($emit_level{node} || $emit_level{aggr})
{
	foreach my $node (keys %phys_h)
	{
		my %tmp_h_node;
		my %tmp_h_aggr;
		foreach my $aggr (keys %{$phys_h{$node}})
		{
			foreach my $vol (keys %{$phys_h{$node}{$aggr}})
			{
				foreach my $counter (keys %{$phys_h{$node}{$aggr}{$vol}})
				{
					# Can only roll-up counters that we know how to...
					next unless (exists $supported_rollup_metric_list{$counter});
				
					# Calc SVM summary data
					if ($counter eq 'read_latency') # So we can do weighted averages
					{
						$tmp_h_aggr{$aggr}{$counter} += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{read_ops});
						$tmp_h_node{$counter}        += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{read_ops});
					}
					elsif ($counter eq 'write_latency') # So we can do weighted averages
					{
						$tmp_h_aggr{$aggr}{$counter} += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{write_ops});
						$tmp_h_node{$counter}        += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{write_ops});
					}
					elsif ($counter eq 'other_latency') # So we can do weighted averages
					{
						$tmp_h_aggr{$aggr}{$counter} += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{other_ops});
						$tmp_h_node{$counter}        += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{other_ops});
					}
					elsif ($counter eq 'avg_latency') # So we can do weighted averages
					{
						$tmp_h_aggr{$aggr}{$counter} += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{total_ops});
						$tmp_h_node{$counter}        += ($phys_h{$node}{$aggr}{$vol}{$counter} * $phys_h{$node}{$aggr}{$vol}{total_ops});
					}
					else # Just sum them up
					{
						$tmp_h_aggr{$aggr}{$counter} += $phys_h{$node}{$aggr}{$vol}{$counter};
						$tmp_h_node{$counter}        += $phys_h{$node}{$aggr}{$vol}{$counter};
					}
				}
			}
		}

		#If aggr level is desired finish math for weighted averages and submit metrics
		if ($emit_level{aggr})
		{
			foreach my $aggr (keys %tmp_h_aggr)
			{
				#Finish math
				$tmp_h_aggr{$aggr}{read_latency}  = $tmp_h_aggr{$aggr}{read_latency}  / $tmp_h_aggr{$aggr}{read_ops}  if ( (exists $tmp_h_aggr{$aggr}{read_ops})  && (exists $tmp_h_aggr{$aggr}{read_latency}) );
				$tmp_h_aggr{$aggr}{write_latency} = $tmp_h_aggr{$aggr}{write_latency} / $tmp_h_aggr{$aggr}{write_ops} if ( (exists $tmp_h_aggr{$aggr}{write_ops}) && (exists $tmp_h_aggr{$aggr}{write_latency}) );
				$tmp_h_aggr{$aggr}{other_latency} = $tmp_h_aggr{$aggr}{other_latency} / $tmp_h_aggr{$aggr}{other_ops} if ( (exists $tmp_h_aggr{$aggr}{other_ops}) && (exists $tmp_h_aggr{$aggr}{other_latency}) );
				$tmp_h_aggr{$aggr}{avg_latency}   = $tmp_h_aggr{$aggr}{avg_latency}   / $tmp_h_aggr{$aggr}{total_ops} if ( (exists $tmp_h_aggr{$aggr}{total_ops}) && (exists $tmp_h_aggr{$aggr}{avg_latency}) );
			
				# Submit aggr counters
				foreach my $counter (keys %{$tmp_h_aggr{$aggr}})
				{
					push @n_emit_items, "$prefix.node.$node.aggr.$aggr.vol_summary.$counter $tmp_h_aggr{$aggr}{$counter} $timestamp";
				}
			}
		}
		
		#If node level is desired finish math for weighted averages and submit metrics
		if ($emit_level{node})
		{
			#Finish math
			$tmp_h_node{read_latency}  = $tmp_h_node{read_latency}  / $tmp_h_node{read_ops}  if ( (exists $tmp_h_node{read_ops})  && (exists $tmp_h_node{read_latency}) );
			$tmp_h_node{write_latency} = $tmp_h_node{write_latency} / $tmp_h_node{write_ops} if ( (exists $tmp_h_node{write_ops}) && (exists $tmp_h_node{write_latency}) );
			$tmp_h_node{other_latency} = $tmp_h_node{other_latency} / $tmp_h_node{other_ops} if ( (exists $tmp_h_node{other_ops}) && (exists $tmp_h_node{other_latency}) );
			$tmp_h_node{avg_latency}   = $tmp_h_node{avg_latency}   / $tmp_h_node{total_ops} if ( (exists $tmp_h_node{total_ops}) && (exists $tmp_h_node{avg_latency}) );
			
			# Submit aggr counters
			foreach my $counter (keys %tmp_h_node)
			{
				push @n_emit_items, "$prefix.node.$node.vol_summary.$counter $tmp_h_node{$counter} $timestamp";
			}
		}
		
	}
}
	
@emit_items = @n_emit_items;
return 0;
