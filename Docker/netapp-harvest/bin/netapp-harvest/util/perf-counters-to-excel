#!/usr/bin/perl

#==================================================================#
#                                                                  #
# Utility script to create Excel spreadsheet from counter manager  #
#  info in Data ONTAP.  Compatible with 7-mode and cDOT            #
#                                                                  #
# Chris Madden, NetApp EMEA                                        #
#                                                                  #
# Copyright (c) 2015 NetApp, Inc. All rights reserved.             #
# Specifications subject to change without notice.                 #
#                                                                  #
#==================================================================#


require 5.6.1;

my $VERSION = '1.2';

use strict;
use warnings;
use FindBin qw($Bin $Script);
use lib "$Bin/../lib";
use Socket;

use Excel::Writer::XLSX;

use NaServer;
use NaElement;
use Getopt::Long;
use File::Basename;

# Pre declare subroutines
sub print_usage;
sub connect_naserver;

# Define any options vars
my %options;

# Get options
GetOptions( \%options, "user=s", "pass=s", "host=s", "v", "h|help|?");

## Print usage and quit if help or bad combo of args is passed
if (exists $options{"h"} || ! exists $options{user} || ! exists $options{pass} || ! exists $options{host})
{
	print_usage;
	exit(0);
}

# Do work!
{
	##
	## Generate Excel filename
	##
	my $filename;
	my @object_names;
	my $server = connect_naserver();
	my $in_version = NaElement->new("system-get-version");
	my $out_version = $server->invoke_elem($in_version);
		
	if($out_version->results_status() eq "failed")
	{
		print("Failed to collect counter information:". $out_version->results_reason() ."\n");
		exit(-2);
	}
	else
	{
		$filename = $out_version->child_get_string('version');
		$filename =~ s/\.//g;
		$filename =~ s/ /_/g;
		($filename) = $filename =~ /NetApp_Release_(.*?):/;
		$filename = "perf_counters_$filename.xlsx";
	}

	##
	## Collect performance object list 
	##
	my $in = NaElement->new("perf-object-list-info");
	my $out = $server->invoke_elem($in);
		
	if($out->results_status() eq "failed")
	{
		print("Failed to collect counter information:". $out->results_reason() ."\n");
		exit(-2);
	}
	
	# Create a new Excel workbook, set format, create main worksheet
	
	my $workbook = Excel::Writer::XLSX->new( $filename ) || die "Cannot create file [$filename] due to reason: $!\n";
	print "Creating $filename\n";
	my $hformat = $workbook->add_format( color => 'blue', underline => 1 );
	my $main = $workbook->add_worksheet( "Object Summary" );

	my $inst_info = $out->child_get("objects");
	my @result = $inst_info->children_get();
	my @fields = qw (name privilege-level description get-instances-preferred-counter filter-data);
	
	# Print header fields
	my $field_num=0;
	foreach (@fields)
	{
		$main->write_string( 0, $field_num, $_ ) ;
		$field_num++;
	}	
	my $format = $workbook->add_format();
    $format->set_text_wrap();

	my $inst_num = 1;	
	
	# Sort object types
	my @sorted_result = sort { $a->child_get_string('name') cmp $b->child_get_string('name') } @result;

	# Iterate through sorted list of object names
	foreach my $inst (@sorted_result)
	{
		my $f_num = 0;
		
		# Iterate through object fields
		foreach my $f (@fields)
		{
			my $val = $inst->child_get_string($f);

			if ($f eq 'name')
			{
				push @object_names, $val;
				my $worksheet_name = substr( $val, 0, 31 );
				$worksheet_name =~ s/:/_/g;

				$main->write_url( $inst_num, $f_num, "internal:$worksheet_name!A1", $hformat, $val);
			}
			else
			{
				$main->write_string( $inst_num, $f_num, $val, $format ) if $val;			
			}
			$f_num++;
		}
		$inst_num++;
	}

	# Set width of object summary worksheet
	$main->set_column( 0, 0 , 35 );
	$main->set_column( 1, 1 , 10 );
	$main->set_column( 2, 2 , 100 );
	$main->set_column( 3, 4 , 30 );
	$main->freeze_panes( 1, 1 );
	
	my $ref_row = 2; # Used for the go to home reference link
	
	# Iterate through each object type and get counter details to build per object worksheets
	foreach my $object_name (@object_names)
	{
		
		my $worksheet_name = substr( $object_name, 0, 31 ); #Max tab length is 32 chars so truncate
		$worksheet_name =~ s/:/_/g; #Colon is not supported as worksheet name so change to underscore
		print " ", $ref_row - 1, " of ", $#object_names +1, ": Creating tab [$worksheet_name]\n";
		my $ws = $workbook->add_worksheet( $worksheet_name);

		my $in2 = NaElement->new("perf-object-counter-list-info");
		$in2->child_add_string("objectname",$object_name);
		my $out2 = $server->invoke_elem($in2);
		
		if($out2->results_status() eq "failed")
		{
			print("Failed to collect counter information:". $out2->results_reason() ."\n");
			exit(-2);
		}

		# Print header		
		my @fields = qw (name desc type is_key privilege-level properties unit base-counter label-info label-info);
		$ws->write_url( 'A1', "internal:'Object Summary'!A$ref_row", $hformat, 'Return to Object Summary Tab');
		my $field_num=0;
		foreach (@fields)
		{
			$ws->write_string( 1, $field_num, $_ ) ;
			$field_num++;
		}
		pop @fields; # We have two headers for label-info, but later on only label-info is looped internally so remove it.
		
		my $format = $workbook->add_format();
		$format->set_text_wrap();
		my $inst_num = 2;	

		# Step through each counter
		my @result = $out2->child_get("counters")->children_get("counter-info");
		
		# Sort object types
		my @sorted_result = sort { $a->child_get_string('name') cmp $b->child_get_string('name') } @result;
		
		foreach my $inst (@sorted_result)
		{
			my $f_num = 0;
			foreach my $f (@fields)
			{
				my $val;
				if ($f eq 'label-info')
				{
					my $label = $inst->child_get("labels") || next;

					# Sometimes there are multiple label-info elements
					for my $label_info ($label->children_get())
					{
						# I have no idea why the following does not work.  I am stumped.  So use sprintf and parse it manually.
						#print "-child is: ".$_->child_get_string('label-info')."\n";
						($val) = $label_info->sprintf =~ /\<label\-info\>(.*)\<\/label\-info\>/;
						$ws->write_string( $inst_num, $f_num, $val, $format ) if $val;
						$f_num++;
					}
				}
				else
				{
					$val = $inst->child_get_string($f);
					$ws->write_string( $inst_num, $f_num, $val, $format ) if $val;
					$f_num++;
				}
			}
			$inst_num++;
		}
		$ws->set_column( 0, 0 , 55 );
		$ws->set_column( 1, 1 , 100 );
		$ws->set_column( 4, 4 , 13 );
		$ws->set_column( 5, 5 , 13 );	
		$ws->set_column( 7, 7 , 12 );
		$ws->set_column( 8, 8 , 100 );	
		$ws->set_column( 9, 9 , 100 );	
		$ws->freeze_panes( 2, 1 );
		$ref_row++;
	}
	
	$workbook->close;
	print "Created $filename\n";
	exit;
}

sub connect_naserver
{
	my $s = NaServer->new ($options{'host'}, 1, 3);
	
	# Check if SDK supports HTTP/1.1 and it is the default version, and for a reverse hostname resolution failure.
	# If both are true it will cause NMSDK failures later (see bug id 881464) and we need to set the less efficient HTTP/1.0
	if ( (eval { $s->get_http_version() }) && ( $s->get_http_version() eq '1.1') )
	{
		if ( ! gethostbyaddr( inet_aton($options{'host'}), AF_INET ))
		{
			$s->set_http_version('1.0');
		}
	}

	my $out = $s->set_transport_type('HTTPS');
	if (ref ($out) eq "NaElement") { 
		if ($out->results_errno != 0) {
			my $r = $out->results_reason();
			print "Connection to $options{'host'} failed: $r\n";
			exit (-2);
		}
	}

		$out = $s->set_style('LOGIN');
	if (ref ($out) eq "NaElement") { 
		if ($out->results_errno != 0) {
			my $r = $out->results_reason();
			print "Connection to $options{'host'} failed: $r\n";
			exit (-2);
		}
	}

	$out = $s->set_admin_user($options{'user'}, $options{'pass'});
	$out = $s->set_port('443');
	return $s;
}

sub print_usage
{
my $basename = $options{basename};

print <<EOF;

Usage: $Script  -host <host> -user <user> -pass <pass> [-h]
 PURPOSE:
  Creates an Excel file named perf_counters_<relname>.xlsx with object and counter details.
  Supports both 7-mode and cDOT
 ARGUMENTS:
   -host         Hostname to connect with
   -user         Username to connect with
   -pass         Password to connect with
 EXAMPLE:
  $Script  -host sdt-cdot1 -user admin -pass secret

EOF

}
		
	
