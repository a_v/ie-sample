This directory should contain .pem and .key files when using certificate
based authentication. For more information please see the NetApp Harvest
Installation and Administration guide.