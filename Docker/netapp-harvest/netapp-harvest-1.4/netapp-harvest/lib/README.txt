Place *.pm files from the NetApp Management SDK into this directory.

Due to licensing reasons you must download the NetApp Management SDK,
and accept those licensing terms, separately from NetApp Harvest.
For more information please see the NetApp Harvest Installation and
Administration guide.