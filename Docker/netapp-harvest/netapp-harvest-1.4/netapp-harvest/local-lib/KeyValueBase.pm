
#===============================================================================#
#                                                                               #
# Base class for key-value based output plugin for NetApp Harves                #
#                                                                               #
# Common implementation of an output plugin based on key-value pair data.       #
#                                                                               #
# Author: Francisco Assis Rosa, NetApp                                          #
#                                                                               #
# Copyright (c) 2017 NetApp, Inc. All rights reserved.                          #
# Specifications subject to change without notice.                              #
#                                                                               #
#===============================================================================#

package KeyValueBase;

=head1 NAME

  KeyValueBase - class encapsulating extraction and outputing key-values from 
  NetApp Harvest

=cut

=head1 DESCRIPTION

  KeyValueBase class implements the methods for configuring key-value
  extraction and for outputing those key-value pairs to an external
  system (e.g. OCI). This is a base class and will not work on it's own.
  It's purpose is to have derived classes from this that implement 
  specific functions that this class relies upon (see Oci.pm).

  The following routines are available for configuration, extraction
  and outputing key-value information to external systems

=cut

use strict;

#============================================================#

=head2  new($name, $config, $poller, $logger, $enabled)

  Construct a new KeyValueBase instance. Parameters:
  
  *) name: name of output plugin class instance.
  *) config: system configuration.
  *) poller: poller configuration.
  *) logger: the logger to use when providing user feedback.
  *) enabled: 1 if plugin is enabled, 0 otherwise
  *) namespaces: comma separated list of namespaces to include in output 

=cut

sub new {
	my ($class) = shift;
	my ($name) = shift;
	my ($config) = shift;
	my ($poller) = shift;
	my ($logger) = shift;
	my ($enabled) = shift;
	my ($namespaces) = shift;

	# create and instantiate attributes for object
	my $self = {
		name => $name,
		enabled => $enabled,
		logger => $logger,
		namespaces => $namespaces,
		storage_name => $config->{display_name}
	};
	# make it an object (bless)
	bless $self, $class;

	# if enabled, load key-value map from configuration and poller info
	if ( $self->{enabled} ) {
		$self->load_key_value_map($config, $poller);
	}

	# done, give created instance back
	return $self;
}

#============================================================#

=head2  emit_items($namespace, $emit_items)

  Emit items to remote system. Aggregates together items
  that relate to same objects as to allow minimization of
  messages from Harvest to remote systems. Parameters:
  
  *) namespace: the namespace for items being emitted
  *) emit_items: the processed items to be sent to remote system.

=cut

sub emit_items($) {
	my $self = shift;
	my $namespace = shift;
	my $emit_items = shift;

	# debug message, number of items to publish
	$self->logger("DEBUG", "[$namespace]:  emit_items, total: " . (@$emit_items));

	# if plugin is not enabled, bail out early
	if ( !$self->{enabled} ) {
		$self->logger("DEBUG", "Bypassing sending items to disabled plugin");
		return;
	}

	# bypass analysis of namespace if not configured to do so
	if ( ! defined $self->{key_value_map}{$namespace} ) {
		$self->logger("DEBUG", "[$namespace] Bypassing sending items to non-configured namespace");
		return;
	}

	# from emit_items we are building a map from
	# namespace (think integrations in OCI, indexes in Elasticsearch for instance)
	# into object ids to aggregated payload for object
	my %namespaceToObjectIdToPayload;

	# look at each emit item
	foreach my $emit (@{$emit_items})
	{
		
		# provide debug info
		$self->logger("DEBUG", "[$namespace][$emit]");

		# emit items are generated with <path> <value> <timestamp> so
		# start by breaking $emit up
		my @item_sections = split(/ /, $emit);
		# grab path from item
		my $item_path = $item_sections[0];
		# grab value from item
		my $item_value = $item_sections[1];
		# grab timestamp from item
		my $item_timestamp = $item_sections[2];
		# proceed if we captured what we need
		if ( $item_timestamp ) {
			# create a hash for item with info gathered from $emit
			my %item_hash;
			# extract a map of key/values from path
			my $item_key_value = $self->get_path_key_values($namespace, $item_path);
			# if we captured any key-value map, proceed
			if ( defined $item_key_value && (keys %$item_key_value) > 0 ) {
				# set default name, all up to last '.' (counter)
				$item_hash{name} = $item_path;
				# grab namespace from key-value map
				my $item_namespace = $item_key_value->{_namespace_};
				# now check all other captured key/value pairs
				for my $oneKey (keys %$item_key_value) {
					my $oneVal = $item_key_value->{$oneKey};
					if ( $oneKey =~ /^_path_counter_$/ ) {
						# grab the counter along with the timestamp
						$item_hash{dataPoints}{sampleTimeUTC} = 1000 * $item_timestamp;
						$item_hash{dataPoints}{$oneVal} = 0 + $item_value;
						# set name for payload (item path - counter name)
						$item_hash{name} = substr($item_path, 0, rindex($item_path, $oneVal) - 1);
					} elsif ( !($oneKey =~ /^_namespace_$/) ) {
						# no explicit tagging means identifier
						$item_hash{identifiers}{$oneKey} = $oneVal;
					}
				}
				# no identifiers at all? this is a singleton, add an artificial id
				if ( ! defined $item_hash{identifiers} ) {
					$item_hash{identifiers}{'id'} = $item_hash{name};
				}
				# add storage name as part of identifiers
				$item_hash{identifiers}{'storage_name'} = $self->{storage_name};

				# accumulate emit items together (e.g. gather all counters that relate to same
				# object in a single payload instead on several payloads)
				$self->accumulate_payload($item_namespace,\%namespaceToObjectIdToPayload, \%item_hash);
			}
		}
	}

	# now once all is accumulated and captured in key-value maps, send them to third party
	# by calling abstract function 'emit_key_values' which is implemented in 
	# sub-classes that know how to send to specific third party systems (see Oci.pm)
	$self->emit_key_values(\%namespaceToObjectIdToPayload);
}


#------------------------------------------------------------#
#
# routines beyond this point are "private"
#
#------------------------------------------------------------#

# Accumulate payloads into as little number of maps as possible 
# (e.g. send counters from same objects in same request instead of several)
# Parameters:
# *) namespace: the namespace for item being accumulated.
# *) namespaceToObjectIdToPayload: map of namespace to object id to payload for 
#    object (where results are to be accumulated).
# *) payload: the payload to accumulate data from.
sub accumulate_payload($@) {
	my $self = shift;
	my $namespace = shift;
	my $namespaceToObjectIdToPayload = shift;
	my $payload = shift;

	# calculate object id from payload (based on identifier attributes)
	my $objectId = $self->get_object_id($payload);
	# if we already seen this object, accumulate values
	if ( $namespaceToObjectIdToPayload->{$namespace}{$objectId} ) {
		# grab existing payload for object
		my $existingPayload = $namespaceToObjectIdToPayload->{$namespace}{$objectId};
		# accumulate name
		if ( $payload->{name} ) {
			$existingPayload->{name} = $payload->{name};
		}
		# accumulate attributes
		if ( $payload->{attributes} ) {
			for my $oneAttribute (keys(%{$payload->{attributes}})) {
				$existingPayload->{attributes}{$oneAttribute} = $payload->{attributes}{$oneAttribute};
			}
		}
		# accumulate dataPoints
		if ( $payload->{dataPoints} ) {
			for my $oneCounter (keys(%{$payload->{dataPoints}})) {
				$existingPayload->{dataPoints}{$oneCounter} = $payload->{dataPoints}{$oneCounter};
			}
		}
	} else {
		# no previous data for object, just grab all from this payload
		$namespaceToObjectIdToPayload->{$namespace}{$objectId} = $payload;
	}
}

# Get object id from a given payload
# Creats unique id from identifiers in payload.
# Parameters:
# *) payload: the payload for one object.
sub get_object_id($@) {
	my $self = shift;
	my $payload = shift;

	# collect all id attribute names from payload
	my @idList;
	foreach my $oneId (keys(%{$payload->{identifiers}})) {
		push(@idList, $oneId);
	}

	# object id to build
	my $objectId = "";
	# sort all id attribute names to ensure id is always the same across payloads
	my @sortedIdsList = sort(@idList);
	foreach my $oneId (@sortedIdsList) {
		# concatenate attribute values to create the full object id
		$objectId = $objectId . "::" . $payload->{identifiers}{$oneId};
	}

	# give it back
	return $objectId;
}

# Load key value map from configuration. This map 
# will drive the translation from Harvest paths into
# key-value pairs suitable to be sent to third party systems.
# Code grabs configuration from template files and creates a Perl
# matching expression along with definition of variable names to be able
# to process incoming paths into key-value pairs.
# example: 
# 	plugin_leaf => 'processor:node.{i_node}.processor.{i_processor}',
#
# will be translated to:
# 	namespace: processor
# 	expression: ^netapp.perf.ntap.aurora\.node\.(.*?)\.processor\.(.*?)\.(.*)$
# 	variables:i_node, i_processor,_path_counter_
#
# Parameters:
# *) config: the system config.
# *) poller: the poller config (with Harvest path definitions and mapping definitions).
sub load_key_value_map($@) {
	my $self = shift;
	my $config = shift;
	my $poller = shift;

	# process namespace inclusion
	my %includedNamespaces;
	$self->logger("DEBUG", "Including namespaces: '$self->{namespaces}'");
	if ( defined $self->{namespaces} ) {
		# split namespaces on ',' then create hash to make it easier to check
		for my $oneIncludedNamespace ( split /,/, $self->{namespaces} ) {
			$includedNamespaces{$oneIncludedNamespace} = 1;
		}
	}

	# look at all definitions in poller
	for my $namespace (keys %$poller) {

		my $includeThisNamespace = (keys %includedNamespaces) == 0 || $includedNamespaces{$namespace};
		if ( ! $includeThisNamespace ) {
			# bypass not included namespace
			#$self->logger("DEBUG", "No keys to process for bypassed namespace: >>$namespace<<");
			next;
		}

		# grab match expression from 'plugin_leaf' first, 'graphite_leaf' if not defined
		my $expressionConfiguration = ($poller->{$namespace}{'plugin_leaf'} ? $poller->{$namespace}{'plugin_leaf'} : $poller->{$namespace}{'graphite_leaf'});
		my @matchExpressionList = ();
		if ( ref($expressionConfiguration) eq 'ARRAY' ) {
			push(@matchExpressionList, @$expressionConfiguration);
		} else {
			push(@matchExpressionList, $expressionConfiguration);
		}

		# process all expressions
		for my $matchExpression (@matchExpressionList) {
			#$self->logger("DEBUG", "Configuration from section: '$namespace', namespace: '$namespace', expression: '$matchExpression', include? $includeThisNamespace");

			# collect the variable list for matches
			my @varList;

			# now grab all variables from matching expression
			while ( $matchExpression =~ m/{(.*?)}/g ) {
				push(@varList, $1);
			}
			# last entry is path counter
			push(@varList, '_path_counter_');

			# handle special chars matching '.'
			$matchExpression =~ s/\./\\./g;
			# transform matching curly braces into group matching in perl
			$matchExpression =~ s/{.*?}/(.*?)/g;
			# add to end of expression the counter match
			$matchExpression = "$matchExpression\\.(.*)\$";

			# add graphite root prefix if existing in match
			if (defined $config->{graphite_root}) {
				$matchExpression = "\^$config->{graphite_root}\\.$matchExpression";
			} else {
				$matchExpression = "\^$matchExpression";
			}

			# add expression as first entry in list
			unshift(@varList, $matchExpression);

			# add list entry for $namespace as needed
			if ( ! defined $self->{key_value_map}{$namespace} ) {
				$self->{key_value_map}{$namespace} = ();
			}
			# store variables list for namespace
			push(@{$self->{key_value_map}{$namespace}}, \@varList);
		}
	}

	# debug output
	if ( defined $self->{key_value_map} ) {
		for my $oneNamespace (keys %{$self->{key_value_map}}) {
			$self->logger("DEBUG", " Namespace: $oneNamespace");
			for my $varList (@{$self->{key_value_map}{$oneNamespace}}) {
				$self->logger("DEBUG", "   Expression: $varList->[0]");
	                	for ( my $varIdx = 1; $varIdx < @{$varList} ; $varIdx++ ) {
					my $oneVarName = $varList->[$varIdx];
					$self->logger("DEBUG", "      Key-value variable: $oneVarName");
		               	 }
       		 	}
		}
	}
}

# Extract key-values based on loadded configuration and based on one path
# Parameter:
# *) namespace: the namespace to parse path for
# *) onePath: the path to extract key-value map from.
sub get_path_key_values($@) {
	my $self = shift;
	my $namespace = shift;
	my $onePath = shift;

	# look at all matching expressions configured in system
	# analyse list in order of definition (first one wins)
	for my $varList (@{$self->{key_value_map}{$namespace}}) {
		my $oneExp = $varList->[0];
		#$self->logger("DEBUG", "[$namespace] Expression: >>$oneExp<<, match against: >>$onePath<<, does it match?");
		# see if path matches
		if ( $onePath =~ m/$oneExp/g ) {
			#$self->logger("DEBUG", "       MATCH!");
			# grab variables for expression
			my %key_value;
			# capture namespace into key-value
			$key_value{_namespace_} = $namespace;
			# grab all variables based on match positioning
	                for ( my $varIdx = 1; $varIdx < @{$varList} ; $varIdx++ ) {
				my $oneVarName = $varList->[$varIdx];
				my $onePath = eval "\$" . $varIdx;
				$key_value{$oneVarName} = $onePath;
			}
			# give back matched map
			return \%key_value;
		}
	}
	#$self->logger("DEBUG", "       NO MATCH! [$namespace][$onePath]");
	# no match found, return undefined key-value map
	return undef;
}

# Logger for class and sub-classes. Encapsulate calls so that we can have 
# a standartized log line with name of plugin clearly identified.
# Parameters:
# *) level: the logging level for message (e.g. DEBUG, NORMAL, WARNING, ERROR).
# *) message: the message being logged.
sub logger($@) {
	my $self = shift;
	my $level = shift;
	my $message = shift;

	# format message, delegate to configured logger
	$self->{logger}->logger($level, "[$self->{name}] $message");
}

# .pm requires a return value
1;
