
#===============================================================================#
#                                                                               #
# NetApp Harvest Logger - logging for  NetApp Harvest                           #
#                                                                               #
# Handles logging to file/stderr/stdout as specified by invoking code           #
#                                                                               #
# Author: Chris Madden, NetApp                                                  #
#                                                                               #
# Copyright (c) 2017 NetApp, Inc. All rights reserved.                          #
# Specifications subject to change without notice.                              #
#                                                                               #
#===============================================================================#

package Logger;

=head1 NAME

  Logger - class encapsulating Netapp Harvest logging requests

=cut

=head1 DESCRIPTION

  A logger encapsulates all logic of providing feedback on
  code execution. It supports sending of message according to 
  different importance level allowing configuration of amount
  of data reported into logging files.

  The following routines are available for constructing and 
  accessing the contents of oci.

=cut

$VERSION = '1.0';	# work with all versions

use strict;
use File::Copy;

#============================================================#

=head2  new($logdir, $options)

	Construct a new Logger. Parameters:

	*) logdir the directory location for generating logfiles to.
	*) options: the logging options as specified in command line params

=cut

sub new {
	my ($class) = shift;
	my ($logdir) = shift;
	my ($options) = shift;

	my $self = {
		logdir => $logdir,
		options => $options
	};

	bless $self, $class;
	return $self;
}

#============================================================#

=head2  logger($level, $message)

	Log messages in a consistent manner. Parameters:

	*) level: the logging granularity level. Values: DEBUG, NORMAL, WARNING, ERROR.
	*) message: the message to be logged

=cut
sub logger($@)
{
	# Logging levels:
	#  DEBUG
	#  NORMAL
	#  WARNING
	#  ERROR
	
	my $self = shift @_;
	my $level = shift @_;		
	return if (($level eq 'DEBUG') && (! exists $self->{options}{'v'})); # Only log DEBUG if verbose enabled
	
	my $datestamp = $self->datestamp(time);
	
	$level = sprintf ("%-7s", $level);
	unless (fileno LOGFILE)
	{
		my $conf = "_$self->{options}{conf}";
		my $logfile = $self->get_log_file();
		
		mkdir $self->{logdir} unless (-d $self->{logdir});
		open (LOGFILE, ">>$logfile");
		open (STDERR,  ">>$logfile");
		select((select(LOGFILE), $|=1)[0]); #Enable autoflush on file handle
	}

	# Print to STDOUT and log file
	print "[$datestamp] [$level] ", @_, "\n";
	print LOGFILE "[$datestamp] [$level] ", @_, "\n"  or print "Write to logfile failed: $!\n";
}
		
#============================================================#

=head2  datestamp($epoch)

	Generate datestamp in a consistent manner Parameters:

	*) epoch: the timestamp to generate consistent representation for.

=cut
sub datestamp($@)
{
	my $self = shift;
	my $epoch = shift;
	my @time = localtime( $epoch );
	$time[5] = $time[5]+1900;
	$time[4] += 1;
	for (@time)
	{
		$_ = "0$_" if (length($_) == 1);
	}
	return ("$time[5]-$time[4]-$time[3] $time[2]:$time[1]:$time[0]");
}

=head2  check_and_rotate_logfile

	Make sure logfiles rotate when reaching size limit

	*) connection: the connection information for log files
=cut
sub check_and_rotate_logfile
{
	my $self = shift;
	my $connection = shift;

	# Get logfile name
	my $logfile = $self->get_log_file();

	if (-s $logfile > ($connection->{logfile_rotate_mb} * 1048576))
	{
		$self->logger ("NORMAL", "[main] Rotating logfile because it has reached [", -s $logfile, "] bytes");

		# Rotate files from n-1 to n
		for (my $n=$connection->{logfile_rotate_keep}; $n > 0; $n--)
		{
			my $src = $logfile.".".($n - 1);
			my $dst = $logfile.".".$n;
			if (-e $src)
			{
				move($src, $dst);
				$self->logger ("NORMAL", "[main] Rotated logfile [$src] to [$dst]");
			}
		}
		# Rotate current log
		close (LOGFILE);
		close (STDERR);
		my $dst = $logfile.".1";
		my $out = move($logfile, $dst);
		$self->logger ("NORMAL", "[main] Rotated logfile [$logfile] to [$dst]");
		$self->logger ("NORMAL", "[main] Logfile rotation complete");
	}
}

=head2  get_log_file

	Access log file name as specified by log dir and options
=cut
sub get_log_file() {
	my $self = shift;

	my $logfile = $self->{logdir}."/".$self->{options}{poller}."_".$self->{options}{conf}.".log";
	$logfile =~ s/\.conf//g;

	return $logfile;
}


