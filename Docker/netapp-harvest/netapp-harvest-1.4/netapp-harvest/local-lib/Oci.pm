
#===============================================================================#
#                                                                               #
# Output plugin for sending data into NetApp OnCommand Insight (OCI)            #
#                                                                               #
# Author: Francisco Assis Rosa, NetApp                                          #
#                                                                               #
# Copyright (c) 2017 NetApp, Inc. All rights reserved.                          #
# Specifications subject to change without notice.                              #
#                                                                               #
#===============================================================================#

package Oci;

=head1 NAME

  Oci - class encapsulating output of data onto NetApp OnCommand Insight (OCI).

=cut

=head1 DESCRIPTION

  An Oci instance will interact with OCI's REST API to send Harvest data.

  The following routines are available for formatting data and sending it to OCI.

=cut

use strict;

# required interaction modules
use JSON;
use LWP::UserAgent;
use LWP::Protocol::https;

# Super class definition (perl inheritance)
use KeyValueBase;
our @ISA = qw(KeyValueBase);

#============================================================#

=head2  new($config, $poller, $logger)

  Construct a new Oci instance. Parameters:
  *) config: system configuration.
  *) poller: poller configuration.
  *) logger: the logger to use in output plugin.

=cut

sub new {
	my ($class) = shift;
	my ($config) = shift;
	my ($poller) = shift;
	my ($logger) = shift;

	# instantiate super class
	my $self = KeyValueBase->new("OCI Output Plugin", $config, $poller, $logger, $config->{oci_enabled}, $config->{oci_namespaces});

	# define attributes specific to Oci instance
	$self->{url} = $config->{oci_url};
	$self->{agent_token} = $config->{oci_agent_token};

	# make it an object (perl bless)
	bless $self, $class;

	# log creation
	$self->logger("NORMAL", "Created, enabled: $self->{enabled}, url: $self->{url}, namespaces: $self->{namespaces}");

	# return instance
	return $self;
}

#------------------------------------------------------------#
#
# routines beyond this point are "private"
#
#------------------------------------------------------------#

# emit a generated values into Oci.
# Parameters:
# *) namespaceToObjectIdToPayload: map of namespace (OCI integration) to object id to payload to send to OCI.
sub emit_key_values($) {
	my $self = shift;
	my $namespaceToObjectIdToPayload = shift;

	# loop through namespaces
	for my $oneNamespace (keys(%$namespaceToObjectIdToPayload)) {
		# grab payloads for namespace
		my $objectIdToPayload = $namespaceToObjectIdToPayload->{$oneNamespace};
		# build json list to send to OCI
		my @json_list;
		for my $oneObjectId (keys(%$objectIdToPayload)) {
			# grab payload for one object
			push(@json_list, $objectIdToPayload->{$oneObjectId});
		}
		# encode json and post via HTTP to OCI
		my $json = encode_json \@json_list;
		$self->post_http_payload($oneNamespace, $json);
	}
}

# Post via HTTP a payload to OCI's REST
# Parameter:
# *) namespace: the namespace to publish payload to.
# *) json: the json payload to send to OCI.
sub post_http_payload($@) {
	my $self = shift;
	my $namespace = shift;
	my $json = shift;

	# replace ':' by '_'
	$namespace =~ s/:/_/g;

	# create user agent for request, bypass SSL verification
	my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0, SSL_verify_mode => 0 });

	# build URL for call, use OCI's REST API for integration data, prefix namespace with 'harvest_'
	my $server_endpoint = $self->{url} . '/rest/v1/integrations/harvest_' . $namespace;

	# debug info
	$self->logger("DEBUG", "Posting to >>$server_endpoint<<, json: >>$json<<");

	# set custom HTTP request header fields
	my $req = HTTP::Request->new(POST => $server_endpoint);
	$req->header('content-type' => 'application/json');
	$req->header('X-OCI-Integration' => $self->{agent_token});
  
	# add POST data to HTTP request body
	$req->content($json);
   
	# fo request, handle response
	my $resp = $ua->request($req);
	if ($resp->is_success) {
		my $message = $resp->decoded_content;
		$self->logger("DEBUG", "Received reply: $message");
	}
	else {
		$self->logger("ERROR", "HTTP POST error code: " . $resp->code);
		$self->logger("ERROR", "HTTP POST error message: " . $resp->message);
		my $message = $resp->decoded_content;
		$self->logger("ERROR", "HTTP POST error Received reply: $message");
	}
}

