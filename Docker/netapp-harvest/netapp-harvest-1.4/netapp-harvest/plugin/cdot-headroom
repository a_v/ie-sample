#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         resource_headroom_cpu, resource_headroom_aggr
# Graphite Leaf:  'node.{node_name}.headroom.processor',     
# Counters req'd: N/A
#
# Description: Calculate remaining headroom
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2016 NetApp, Inc. All rights reserved.             
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================


use strict;
use warnings;

# Declare our globals
our %poller;
our %connection;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");
						
my @n_emit_items;
my %h;
my $timestamp;

# Rewrite metrics:

# IN: netapp.perf.dev.blob1.node.mt_stc4009.headroom.processor.ewma_hourly.ops 593 1471384590
# IN: netapp.perf.dev.blob1.node.mt_stc4009.headroom.processor.current_ops 0.6 1471384570
# IN: netapp.perf.dev.blob1.node.mt_stc4009.headroom.aggr.DISK_HDD_n1_root_24cf3f4d-726d-437d-85c5-52d953ba55fc.ewma_weekly.optimal_point_utilization 69 1471388900
#     start----------------------------------------> type---> counter->       val-> ts------->

foreach my $item (@emit_items)
{
	#logger ("DEBUG", "[$obj] IN: $item");
	my ($start, $type, $counter, $value, $ts) = $item =~ /(.*\.headroom)\.(.*?)\.(.*?) (.*) (\d+)/;
	$timestamp = $ts;
	my $out;
	
	# Scale ewma latency counters since they are in an array
	$value = normalize_units($value, 'microsec') if ($counter =~ /ewma_.*latency/);

	if ($type eq 'aggr')
	{
		my $aggr;
		my $disk_type;
		($disk_type, $aggr, $counter) = $counter =~ /DISK_([HS][SD]D)_(.*)_\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12}\.(.*)/;
		my $prefix = "$start.$type.$aggr.$disk_type";
		$h{$prefix}{$counter} = $value;
		$out = "$prefix.$counter $value $ts";
	}
	else #processor
	{
		my $prefix = "$start.$type";
		$h{$prefix}{$counter} = $value;
		$out = "$prefix.$counter $value $ts";
	}
	
	push @n_emit_items, $out;
	#logger ("DEBUG", "[$obj] OUT: $out");
}

# Create remaining counters
foreach my $prefix (keys %h)
{
	# Handle current values
	{
		# Remaining current ops
		if ( (exists $h{$prefix}{'optimal_point_ops'}) && (exists $h{$prefix}{'current_ops'}) )
		{
			push @n_emit_items, "$prefix.remaining_ops ".($h{$prefix}{'optimal_point_ops'} - $h{$prefix}{'current_ops'})." $timestamp";
		}
		# Remaining current utilization
		if ( (exists $h{$prefix}{'optimal_point_utilization'}) && (exists $h{$prefix}{'current_utilization'}) )
		{
			push @n_emit_items, "$prefix.remaining_utilization ".($h{$prefix}{'optimal_point_utilization'} - $h{$prefix}{'current_utilization'})." $timestamp";
		}
	}
	
	# Handle archives (which have slightly different names)
	foreach my $cnt ('ewma_hourly.', 'ewma_daily.', 'ewma_weekly.', 'ewma_monthly.')
	{
		# Remaining ops
		if ( (exists $h{$prefix}{$cnt.'optimal_point_ops'}) && (exists $h{$prefix}{$cnt.'ops'}) )
		{
			push @n_emit_items, "$prefix.${cnt}remaining_ops ".($h{$prefix}{$cnt.'optimal_point_ops'} - $h{$prefix}{$cnt.'ops'})." $timestamp";
		}
		# Remaining utilization
		if ( (exists $h{$prefix}{$cnt.'optimal_point_utilization'}) && (exists $h{$prefix}{$cnt.'utilization'}) )
		{
			push @n_emit_items, "$prefix.${cnt}remaining_utilization ".($h{$prefix}{$cnt.'optimal_point_utilization'} - $h{$prefix}{$cnt.'utilization'})." $timestamp";
		}
	}

}

@emit_items = @n_emit_items;

return 0;
