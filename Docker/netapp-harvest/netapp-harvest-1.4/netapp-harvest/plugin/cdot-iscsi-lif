#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         iscsi_lif
# Graphite Leaf:  'svm.{vserver_name}.{node_name}.{instance_name}'
# Counters req'd: N/A
#                 Only supports SVM roll-up for
#                 iscsi_read_ops avg_read_latency
#				  iscsi_write_ops	avg_write_latency
#				  iscsi_other_ops avg_other_latency
#				  cmd_transfered avg_latency
#				  read_data write_data
# Plugin Options:  'svm_lif' => [0|1] (enables/disables metrics at SVM fcp_lif level; default is enabled)
#                  'svm_protocol' => [0|1] (enables/disables metrics at SVM fcp protocol level; default is enabled)
#                  'node_protocol' => [0|1] (enables/disables metrics at node fcp protocol level; default is enabled)
#
# Description: Rewrite metric path for usability.  Create SVM iscsi metrics.
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2015 NetApp, Inc. All rights reserved.             
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================

# Note, as of 8.3RC1 the current_port counter is not populated correctly preventing iscsi_port summarized counters.
# Because this should be fixed later I am leaving related code in place but commenting it out
# with '#X#'.  Later when this is fixed updating the plugin should be easier.
#

use strict;
use warnings;

# Declare our globals
our %poller;
our %connection;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");
						
my @n_emit_items;
my %svm_lif_h;    # per SVM per LIF
my %svm_h;        # per SVM
#X# my %node_port_h;  # per node per port
my %node_h;       # per node
my %svm_node_map; # map of svm/lif to node/port
my ($prefix, $timestamp); #Timestamp and prefix for all summarized metrics

# Manage plugin options (default to show all, but allow for less)
my $emit_per_svm_lif       = 1;
my $emit_per_svm_protocol  = 1;
#X# my $emit_per_node_port     = 0;
my $emit_per_node_protocol = 1;
$emit_per_svm_lif       = $poller{$obj}{plugin_options}{'svm_lif'}       if (exists $poller{$obj}{plugin_options}{'svm_lif'}) ;
$emit_per_svm_protocol  = $poller{$obj}{plugin_options}{'svm_protocol'}  if (exists $poller{$obj}{plugin_options}{'svm_protocol'}) ;
#X# $emit_per_node_port     = $poller{$obj}{plugin_options}{'node_port'}     if (exists $poller{$obj}{plugin_options}{'node_port'}) ;
$emit_per_node_protocol = $poller{$obj}{plugin_options}{'node_protocol'} if (exists $poller{$obj}{plugin_options}{'node_protocol'}) ;
	
#List of counters eligible for roll-up SVM
my %supported_rollup_metric_list = map { $_ => 1 } qw(
														iscsi_read_ops avg_read_latency
														iscsi_write_ops	avg_write_latency
														iscsi_other_ops avg_other_latency
														cmd_transfered avg_latency
														read_data write_data);


# Rewrite metrics:
      
# IN: dev.sdt-cdot1.svm.sdt-vs-san1.sdt-cdot1-01.sdt-vs-san1_sdt-vs-san1-iscsi1.avg_read_latency 22.8723731933153 1412845942
#     start------->     svmname->   node-------> lif--------------------------> counter--------> value----------> ts------->

# For iscsi_lif
#OUT: dev.sdt-cdot1.svm.sdt-vs-san1.iscsi-lif.sdt-vs-san1-02-iscsi2.read_data 29536.0348011364 1416910240
#     start------->     svmname--->         lif-------------------> counter-> value----------> ts------->

# For SVM 
#OUT: dev.sdt-cdot1.svm.sdt-vs-san1.iscsi.read_data 29536.0348011364 1416910240
#     start------->     svmname--->       counter-> value----------> ts------->

# For iscsi_port
#OUT: dev.sdt-cdot1.node.sdt-cdot1-02.iscsi_port.a0a-900.read_data 29536.0348011364 1416910240
#     start------->      node------->            port--> counter-> value----------> ts------->

# For Node
#OUT: dev.sdt-cdot1.node.sdt-cdot1-02.iscsi.read_data 29536.0348011364 1416910240
#     start------->      node------->       counter-> value----------> ts------->

foreach my $item (@emit_items)
{
	#logger ("DEBUG", "[$obj] IN: $item");
	my ($start, $svm, $node, $lif, $counter, $value, $ts) = $item =~ /(.*)\.svm\.(.*?)\.(.*?)\.(.*?)\.(.*?) (.*) (\d+)/;

	# Setup stuff for iscsi counters
	$prefix = $start;
	$timestamp = $ts;

	#Add data points, node, and port to hashes
	$svm_lif_h{$svm}{$lif}{$counter} = $value;
	#X# $svm_node_map{$svm}{$lif}{port}  = $port; 
	$svm_node_map{$svm}{$lif}{node}  = $node;
}

# Step through LIF metrics and emit them, and populate hashes for SVM, node, and node port
foreach my $svm (keys %svm_lif_h)
{
	foreach my $lif (keys %{$svm_lif_h{$svm}})
	{
		foreach my $counter (keys %{$svm_lif_h{$svm}{$lif}})
		{
			# Grab node and port for lif
			my $node = $svm_node_map{$svm}{$lif}{node};
			#X# my $port = $svm_node_map{$svm}{$lif}{port};
			
			# Add SVM lif metrics (if requested)
			push @n_emit_items, "$prefix.svm.$svm.iscsi_lif.$lif.$counter $svm_lif_h{$svm}{$lif}{$counter} $timestamp" if ($emit_per_svm_lif); 

			# Can only roll-up counters that we know how to...
			next unless (exists $supported_rollup_metric_list{$counter});			
		
			# Calc SVM summary data
			if ($counter eq 'avg_read_latency') # So we can do weighted averages
			{
				$svm_h{$svm}{$counter}                 += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_read_ops});
				#X# $node_port_h{$node}{$port}{$counter}   += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_read_ops});
				$node_h{$node}{$counter}               += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_read_ops});
			}                                                                                                        
			elsif ($counter eq 'avg_write_latency') # So we can do weighted averages                                 
			{                                                                                                        
				$svm_h{$svm}{$counter}                 += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_write_ops});
				#X# $node_port_h{$node}{$port}{$counter}   += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_write_ops});
				$node_h{$node}{$counter}               += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_write_ops});
			}
			elsif ($counter eq 'avg_other_latency') # So we can do weighted averages
			{
				$svm_h{$svm}{$counter}                 += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_other_ops});
				#X# $node_port_h{$node}{$port}{$counter}   += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_other_ops});
				$node_h{$node}{$counter}               += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{iscsi_other_ops});
			}
			elsif ($counter eq 'avg_latency') # So we can do weighted averages
			{
				$svm_h{$svm}{$counter}                 += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{cmd_transfered});
				#X# $node_port_h{$node}{$port}{$counter}   += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{cmd_transfered});
				$node_h{$node}{$counter}               += ($svm_lif_h{$svm}{$lif}{$counter} * $svm_lif_h{$svm}{$lif}{cmd_transfered});
			}
			else # Just sum them up
			{
				$svm_h{$svm}{$counter}                 += $svm_lif_h{$svm}{$lif}{$counter};
				#X# $node_port_h{$node}{$port}{$counter}   += $svm_lif_h{$svm}{$lif}{$counter};
				$node_h{$node}{$counter}               += $svm_lif_h{$svm}{$lif}{$counter};
			}
		}
	}
}

# Emit SVM protocol level (if requested)
if ($emit_per_svm_protocol)
{
	foreach my $svm (keys %svm_h)
	{
		# Finish computing weighted averages for latency counters
		$svm_h{$svm}{avg_read_latency}  = $svm_h{$svm}{avg_read_latency}  / $svm_h{$svm}{iscsi_read_ops}  if (exists $svm_h{$svm}{iscsi_read_ops});
		$svm_h{$svm}{avg_write_latency} = $svm_h{$svm}{avg_write_latency} / $svm_h{$svm}{iscsi_write_ops} if (exists $svm_h{$svm}{iscsi_write_ops});
		$svm_h{$svm}{avg_other_latency} = $svm_h{$svm}{avg_other_latency} / $svm_h{$svm}{iscsi_other_ops} if (exists $svm_h{$svm}{iscsi_other_ops});
		$svm_h{$svm}{avg_latency}       = $svm_h{$svm}{avg_latency}       / $svm_h{$svm}{cmd_transfered} if (exists $svm_h{$svm}{cmd_transfered});
	
		foreach my $counter (keys %{$svm_h{$svm}})
		{
			push @n_emit_items, "$prefix.svm.$svm.iscsi.$counter $svm_h{$svm}{$counter} $timestamp";
		}
	}
}

# Emit node protocol level (if requested)
if ($emit_per_node_protocol)
{
	foreach my $node (keys %node_h)
	{
		# Finish computing weighted averages for latency counters
		$node_h{$node}{avg_read_latency}  = $node_h{$node}{avg_read_latency}  / $node_h{$node}{iscsi_read_ops}  if (exists $node_h{$node}{iscsi_read_ops});
		$node_h{$node}{avg_write_latency} = $node_h{$node}{avg_write_latency} / $node_h{$node}{iscsi_write_ops} if (exists $node_h{$node}{iscsi_write_ops});
		$node_h{$node}{avg_other_latency} = $node_h{$node}{avg_other_latency} / $node_h{$node}{iscsi_other_ops} if (exists $node_h{$node}{iscsi_other_ops});
		$node_h{$node}{avg_latency}       = $node_h{$node}{avg_latency}       / $node_h{$node}{cmd_transfered} if (exists $node_h{$node}{cmd_transfered});
	
		foreach my $counter (keys %{$node_h{$node}})
		{
			push @n_emit_items, "$prefix.node.$node.iscsi.$counter $node_h{$node}{$counter} $timestamp";
		}
	}
}

=cut

#X# Section for node port processing after fix


# Emit node port level (if requested)
if ($emit_per_node_port)
{
	foreach my $node (keys %node_port_h)
	{
		foreach my $port (keys %{$node_port_h{$node}})
		{
			# Finish computing weighted averages for latency counters
			$node_port_h{$node}{$port}{avg_read_latency}  = $node_port_h{$node}{$port}{avg_read_latency}  / $node_port_h{$node}{$port}{iscsi_read_ops}  if (exists $node_port_h{$node}{$port}{iscsi_read_ops});
			$node_port_h{$node}{$port}{avg_write_latency} = $node_port_h{$node}{$port}{avg_write_latency} / $node_port_h{$node}{$port}{iscsi_write_ops} if (exists $node_port_h{$node}{$port}{iscsi_write_ops});
			$node_port_h{$node}{$port}{avg_other_latency} = $node_port_h{$node}{$port}{avg_other_latency} / $node_port_h{$node}{$port}{iscsi_other_ops} if (exists $node_port_h{$node}{$port}{iscsi_other_ops});
			$node_port_h{$node}{$port}{avg_latency}       = $node_port_h{$node}{$port}{avg_latency}       / $node_port_h{$node}{$port}{cmd_transfered} if (exists $node_port_h{$node}{$port}{cmd_transfered});
		
			foreach my $counter (keys %{$node_port_h{$node}{$port}})
			{
				push @n_emit_items, "$prefix.node.$node.fcp_port.$port.$counter $node_port_h{$node}{$port}{$counter} $timestamp";
			}
		}
	}
}
=cut

@emit_items = @n_emit_items;
return 0;

