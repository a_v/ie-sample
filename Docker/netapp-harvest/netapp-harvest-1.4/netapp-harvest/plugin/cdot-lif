#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         lif
# Graphite Leaf:  'svm.{vserver_name}.{node_name}.{instance_name}'      
# Counters req'd: N/A
#
# Description: Rewrite metric path for usability
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2015 NetApp, Inc. All rights reserved.             
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================

use strict;
use warnings;

# Declare our globals
our %poller;
our %connection;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");
						
my @n_emit_items;

# Rewrite metrics:
      
#IN 8.2: ntap-perf.dev.sdt-cdot1.svm.sdt-vs-nas1.sdt-cdot1-01.sdt-vs-nas1:sdt-vs-nas1_cifs_nfs_lif2.recv_data 0.945933948863636 1421138070
#IN 8.3: ntap-perf.dev.blob1.svm.Cluster.blob1-01.blob1-01_clus1.recv_data 0.0870669682820638 1421138270    
#IN 8.3: ntap-perf.dev.blob1.svm.blob1.blob1-02.blob2_ic.recv_data 6.78168402777778e-006 1421138270         
#IN 8.3: ntap-perf.dev.blob1.svm.aspserver-nfs.blob1-01.asp-vmware-03.recv_data 0.000606960720486111 1421138270
#        start-----------------> svm---------> node---> lif---------> counter->

#OUT   : ntap-perf.dev.blob1.svm.blob1-02.lif.blob2_ic.recv_data 6.78168402777778e-006 1421138270

foreach my $item (@emit_items)
{
	#logger "DEBUG", "IN: $item";
	my ($start, $svm, $node, $lif, $end, $val, $ts) = $item =~ /(.*)\.svm\.(.*?)\.(.*?)\.(.*?)\.(.*) (.*) (\d+)/;
	
	# In 8.2 the instance name (lif) was pre-appended with the svm. If true strip it off.
	if ($lif =~ /.*:(.*)/)
	{
		$lif = $1;
	}
	
	# In 8.3 cluster lifs are owned by 'Cluster' SVM, and node mgmt and IC are owned by the admin SVM
	# Because we prefer to associate these non-moveable lifs with the node they belong to rewrite metrics
	# path to the approriate admin SVM as was the case in 8.2.  Cluster mgt lif does not (as of 8.3) increment
	# counters and thus will not show anywhere.  If these counters begin incrementing in a future release the
	# metrics will be submitted under the current node owner.
	if ( ($svm eq 'Cluster') || ($svm eq $connection{cluster_name}) )
	{
		$svm = $node;
	}
	
	my $n_item = "$start.svm.$svm.lif.$lif.$end $val $ts";
	#logger "DEBUG", "OUT: $n_item";
	push @n_emit_items, $n_item;
}

@emit_items = @n_emit_items;
return 0;
