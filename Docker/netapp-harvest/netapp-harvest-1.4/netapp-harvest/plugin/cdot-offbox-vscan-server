#==================================================================
#                                                                  
# Performance counter poller for Data ONTAP plugin                 
# OS:             cDOT
# Object:         offbox_vscan_server
# Graphite Leaf:  'vscan.{instance_name}'
# Plugin Options:  'scanner'    => [0|1] (enables/disables metrics per SVM; default is enabled)
#                  'svm_scanner_node' => [0|1] (enables/disables metrics per SVM-Scanner-Node; default is disabled)
#
# Counters req'd: 
#
#   These counters are per scanner and can be passed in by averaging:
#   scanner_stats_pct_cpu_used
#   scanner_stats_pct_mem_used
#   scanner_stats_pct_network_used
#
#   These counters need weighted averages:
#   scan_latency
#   scan_latency_base
#
#   These counters need to be summed:
#   scan_request_dispatched_rate
#
# Description: Roll-up offbox_vscan_server stats
#                                                                  
# Author: Chris Madden, NetApp                                     
#                                                                  
# Copyright (c) 2017 NetApp, Inc. All rights reserved.             
# Specifications subject to change without notice.                 
#                                                                  
#==================================================================

use strict;
use warnings;

# Declare our globals
our %poller;
our @emit_items;

my $obj = shift;

logger ("DEBUG", "[$obj] plugin starting [$poller{$obj}{plugin}]");


# Manage plugin options setting defaults
my %emit_level = (scanner => '1', svm_scanner_node => '0');

# Allow tweaking in poller config
for (keys %{$poller{$obj}{plugin_options}})
{
	$emit_level{$_} = $poller{$obj}{plugin_options}{$_};
}

						
my @n_emit_items;

# Rewrite metrics:
      
# Rewrite metrics:
# INCOMING: netapp.perf.dev.nltl-fas2520.vscan.madden-svm1:10_64_30_62:nltl-fas2520-02.scanner_stats_pct_mem_used 18 1501765640
#           start----------------->                svm-------> scanner---> node----------> counter------------>       val------------> ts------->

# OUTGOING per scanner =
#           netapp.perf.dev.nltl-fas2520.vscan.scanner.10_64_30_62.scanner_stats_pct_mem_used 18 1501765640
#
# OUTGOING per svm_scanner_node =
#           netapp.perf.dev.nltl-fas2520.vscan.svm.madden-svm1.scanner.10_64_30_62.node.nltl-fas2520-02.scanner_stats_pct_mem_used 18 1501765640


my ($prefix, $timestamp, %h_scanner);

# Rewrite metrics slightly and populate hash
foreach my $item (@emit_items)
{
	# logger ("DEBUG", "[$obj] IN: $item");
	my ($start, $svm, $scanner, $node, $counter, $value, $ts) = $item =~ /(.*)\.vscan\.(.*?):(.*?):(.*?)\.(.*?) (.*) (\d+)/;

	push @n_emit_items, "$start.vscan.svm.$svm.scanner.$scanner.node.$node.$counter $value $ts" if ($emit_level{svm_scanner_node});

	$prefix = $start;
	$timestamp = $ts;
	$h_scanner{$scanner}{$svm}{$node}{$counter} = $value;
}

if ($emit_level{scanner})
{

	# Prepare and submit weighted average for scanner
	foreach my $scanner (keys %h_scanner)
	{
		my ($ops, $latency, $scan_request_dispatched_rate);
		my (%h_avg, %h_avg_cnt);
	
		foreach my $svm (keys %{$h_scanner{$scanner}})
		{
			foreach my $node (keys %{$h_scanner{$scanner}{$svm}})
			{
				# Prepare math for "weighted average"
				if ( ( exists $h_scanner{$scanner}{$svm}{$node}{'scan_latency'}) || ( exists $h_scanner{$scanner}{$svm}{$node}{'scan_latency_base'}) ) 
				{
					$latency += $h_scanner{$scanner}{$svm}{$node}{'scan_latency_base'} * $h_scanner{$scanner}{$svm}{$node}{'scan_latency'};
					$ops     += $h_scanner{$scanner}{$svm}{$node}{'scan_latency_base'};
				}
	
				# Prepare math for "sum"
      	$scan_request_dispatched_rate += $h_scanner{$scanner}{$svm}{$node}{'scan_request_dispatched_rate'} if exists ($h_scanner{$scanner}{$svm}{$node}{'scan_request_dispatched_rate'});
	
				# Prepare for "avg"
				foreach my $c ( qw(scanner_stats_pct_cpu_used scanner_stats_pct_mem_used scanner_stats_pct_network_used ) )
				{
					next unless (exists $h_scanner{$scanner}{$svm}{$node}{$c});
					$h_avg{$c} += $h_scanner{$scanner}{$svm}{$node}{$c};
					$h_avg_cnt{$c}++;
				}
	
			}
		}
	
		# Submit values where math was "weighted average"
		if ($ops > 0)
		{
			my $scan_latency = $latency / $ops;
			push @n_emit_items, "$prefix.vscan.scanner.$scanner.scan_latency $scan_latency $timestamp";
			push @n_emit_items, "$prefix.vscan.scanner.$scanner.scan_latency_base $ops $timestamp";
		}
	
		# Submit values where math was "sum"
		push @n_emit_items, "$prefix.vscan.scanner.$scanner.scan_request_dispatched_rate $scan_request_dispatched_rate $timestamp" if (defined $scan_request_dispatched_rate);
	
		# Submit values where "avg"
		foreach my $c (keys %h_avg)
		{
			my $val = $h_avg{$c} / $h_avg_cnt{$c};
			push @n_emit_items, "$prefix.vscan.scanner.$scanner.$c $val $timestamp";
		}
	}
}


@emit_items = @n_emit_items;
return 0;
