##
## 7.3 sample configuration template
##
%poller =   (
##
## Basic system level counters
##
	'system' =>
			{ 
				counter_list     => [ qw(
									sys_write_latency sys_read_latency
									read_ops write_ops total_ops
									avg_processor_busy  cpu_elapsed_time cpu_elapsed_time1
									cifs_ops nfs_ops iscsi_ops fcp_ops
									disk_data_read disk_data_written
									net_data_recv net_data_sent
									) ],
				graphite_leaf    => 'system',
				enabled              => '1'
			},
	'wafl' =>
			{ 
				counter_list     => [ qw(
									total_cp_msecs cp_count cp_phase_times avg_wafl_msg_latency
									) ],
				graphite_leaf    => 'wafl',
				enabled              => '1'
			},
##
## Physical counters
##	
	# CPU and domain info
	'processor' =>
			{ 
				counter_list     => [ qw(domain_busy
									processor_busy  ) ],
				graphite_leaf    => 'processor.{instance_name}',
				plugin           => '7dot-processor',
				plugin_leaf	 => [ 'processor.{instance_name}',
									'processor'
									],
				plugin_options   => {'cpu' => 0, 'domain' => 1},
				enabled              => '1'
			},
	# Disk activity at the node/aggr/plex/raidgroup/disk level.  Each level is calculated as the max value of any
	# instance at the lower layer in the hierarchy.  So the raidgroup disk_busy will be the max disk busy of any disk
	# in the raidgroup, and plex the busiest of the two plexes, and aggr, the busiest of the plexes.
	# It is recommended to not collect at the disk level because (a) doing so consumes a bunch of disk space and
	# you rarely care about the 5yr trend of a specific disk and (b) disks get renamed as paths change and you
	# will end up with 'stale' files that need to be merged or deleted.  Better to avoid them in the first place.
	'disk' =>
			{ 
				counter_list     => [ qw( raid_name disk_speed 
									disk_busy raid_type cp_reads user_reads user_writes io_pending io_queued total_transfers
									user_read_chain user_write_chain cp_read_chain user_read_latency user_write_latency cp_read_latency
									) ],
				graphite_leaf    => 'aggr.{raid_name}.{raid_type}.{disk_speed}',
				plugin           => '7dot-disk',
				plugin_leaf	 => [ 'aggr.{aggr}.{plex}.{raid_group}.{disk}',
									'aggr.{aggr}.{plex}.{raid_group}',
									'aggr.{aggr}.{plex}',
									'aggr.{aggr}',
									'aggr',
									],
				plugin_options   => {'disk' => 0, 'rg' => 1, 'plex' => 1, 'aggr' => 1, 'node' => 1},
				enabled          => '1'
			},
	# Disk / Tape adapters
	'hostadapter' =>
			{
				counter_list 	=> [ qw( 
									bytes_read bytes_written
									) ],
				graphite_leaf => 'hostadapter.{instance_name}',
				enabled => '1'
			},			
	# FCP port activity
	'target' =>
			{ 
				counter_list     => [ qw(read_data read_ops write_data write_ops
									) ],
				graphite_leaf    => 'fcp_port.{instance_name}',
				enabled          => '1'							
			},
	# Ethernet port activity
	'ifnet' =>
			{ 
				counter_list     => [ qw(send_data recv_data
									) ],
				graphite_leaf    => 'eth_port.{instance_name}',
				enabled          => '1'							
			},
	# Flash Cache activity
	'ext_cache_obj' =>
			{ 
				counter_list     => [ qw( usage disk_reads_replaced accesses hit_percent
										inserts evicts invalidates
										hit hit_normal_lev0 hit_metadata_file hit_directory	hit_indirect
										miss miss_normal_lev0 miss_metadata_file	miss_directory miss_indirect
									) ],
				graphite_leaf    => 'flashcache.{instance_name}',
				enabled              => '1'
			},
##
## Logical resource counters
##
	'volume' =>
			{ 
				counter_list     => [ qw(
									read_data write_data wv_fsinfo_blks_used 
									read_ops write_ops other_ops total_ops
									read_latency write_latency other_latency avg_latency
									) ],
				graphite_leaf    => 'vol.{instance_name}',
				plugin           => '7dot-volume',					
				enabled          => '1'
			},
	'lun' =>
			{ 
				counter_list     => [ qw( display_name
									read_data write_data 
									read_ops write_ops other_ops
									avg_latency
									read_partial_blocks write_partial_blocks 
									write_align_histo scsi_partner_data scsi_partner_ops
									) ],
				graphite_leaf    => '{instance_name}',
				plugin           => '7dot-lun',
				plugin_leaf	 => 'vol.{volume}.lun.{lun}',
				enabled          => '1'
			},
##
## Basic system level counters
##
	# FCP protocol monitoring
	'fcp' =>
			{ 
				counter_list     => [ qw( fcp_read_data fcp_read_ops fcp_read_latency
									 fcp_write_data fcp_write_ops fcp_write_latency
									 fcp_ops fcp_latency
									) ],
				graphite_leaf    => 'fcp',
				enabled          => '1'							
			},          
	# iSCSI protocol monitoring
	'iscsi' =>
			{ 
				counter_list     => [ qw( iscsi_read_data iscsi_read_ops iscsi_read_latency
									 iscsi_write_data iscsi_write_ops iscsi_write_latency
									 iscsi_ops iscsi_latency
									) ],
				graphite_leaf    => 'iscsi',
				enabled          => '1'							
			}, 
	# NFSv3 protocol monitoring
	'nfsv3' =>
			{
				counter_list     => [ qw(nfsv3_ops nfsv3_avg_op_latency 
										nfsv3_op_latency nfsv3_op_count 
                                    ) ],
                graphite_leaf    => 'nfsv3',
				enabled              => '1'
            },
	# CIFS protocol monitoring
	# Note, cifs_op_pct counter appears to be inaccurate and for this reason is not listed
	'cifs' =>
			{ 
				counter_list     => [ qw( 
									cifs_op_count
									cifs_ops cifs_read_ops cifs_write_ops
									cifs_latency cifs_read_latency cifs_write_latency
									
									) ],
				graphite_leaf    => 'cifs',
				plugin           => '7dot-cifs',
				enabled          => '1'
			},
	# Authentication performance monitoring; see KB 1013398 for definitions
	'cifsdomain' =>
			{ 
				counter_list     => [ qw( netlogon_latency samr_latency lsa_latency
									) ],
				graphite_leaf    => 'cifs.auth.{instance_name}',
				plugin           => '7dot-cifsdomain',
				plugin_leaf	 => [ 'cifs.auth.{svm}.{dc}',
									'cifs.auth.{svm}'
									],
				enabled          => '1'
			},
	# pBlk monitoring; see KB 1013397 for definitions
	'cifs_stats' =>
			{ 
				counter_list     => [ qw( curr_mem_ctrl_blk_cnt max_mem_ctrl_blk_cnt exhaust_mem_ctrl_blk_cnt exhaust_mem_ctrl_blk_reserve_cnt 
							              auth_qlength max_auth_qlength offload_qlength max_offload_qlength
										  curr_sess_cnt curr_cred_cnt curr_lock_cnt
									) ],
				graphite_leaf    => 'cifs.{instance_name}',
				plugin           => '7dot-cifs-stats',
				plugin_leaf	 => [ 'cifs.auth.{vfiler}', 'cifs' ],
				enabled          => '1'
			},
	# VSCAN monitoring; see KB 1013401 for definitions
	'vscan_stats' =>
			{ 
				counter_list     => [ qw( scantime_avg_latency scanrequests_current scanrequests_throttled_current scanrequests_completed
									) ],
				graphite_leaf    => 'cifs.vscan.{instance_name}',
				enabled          => '1'
			},
	# VSCAN monitoring per VSCAN server; see KB 1013401 for definitions
	'vscan_server_stat' =>
			{ 
				counter_list     => [ qw( vscan_ops_server_latency vscan_server_latency scanrequests_current_server scancompletions_from_server_rate
									) ],
				graphite_leaf    => 'cifs.vscan.{instance_name}',
				plugin           => '7dot-vscan-server-stat',
				plugin_leaf	 => 'cifs.vscan.{svm}.{dc}',
				enabled          => '1'
			},


);

##
## Counter definition overrides
##
## Some counter definitions in Data ONTAP are defined incorrectly. This section allows overriding the
## metadata config from Data ONTAP enabling standardized processing and calculation in harvest. 
##
%override = (
				vstorage =>
				{
					xcopy_total_data          => { unit => kb_per_sec },
					writesame_total_data      => { unit => kb_per_sec },
					xcopy_total_data          => { properties => rate },
					writesame_total_data      => { properties => rate },
					xcopy_copy_reqs		      => { properties => rate },
					writesame_reqs 		      => { properties => rate },        
					writesame_holepunch_reqs  => { properties => rate },
					vaw_reqs		          => { properties => rate },
				},
				'cifs' =>
				{
					cifs_op_count 	   	   => { properties => rate, unit => per_sec },
				},
				'nfsv3' =>
				{
					nfsv3_op_count 	   	   => { properties => rate, unit => per_sec },
				},
				'volume' =>
				{
					wv_fsinfo_blks_used	   => { unit => wafl_block },
				},
				'hostadapter' =>
				{
					bytes_read             => { unit => b_per_sec },
					bytes_written          => { unit => b_per_sec },
				},
			);
