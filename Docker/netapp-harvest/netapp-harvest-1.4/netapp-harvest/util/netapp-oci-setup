#!/usr/bin/perl
#===============================================================================#
# OnCommand Insight (OCI) setup utility for using with Harvest.
#                                                                               
# Author: Francisco Assis Rosa, NetApp                                          #
#
# Copyright (c) 2017 NetApp, Inc. All rights reserved.                          #
# Specifications subject to change without notice.                              #
#===============================================================================#

use strict;

use FindBin qw($Bin $Script);
use Getopt::Long;
use LWP::UserAgent;
use LWP::Protocol::https;
use MIME::Base64;

# Pre declare subroutines
sub missingRequiredOptions($@);
sub extractAgentIdFromResponseJson($@);
sub logger($@);

# Script variables
our %options;    # Holds CLI options passed to the script

# Process options from command line
GetOptions( \%options, "oci_agent_token=s", "oci_url=s", "oci_user=s", "oci_password=s", "oci_namespaces=s", "h|help|?", "v");

## Print usage and quit if help or bad combo of args is passed
if (exists $options{"h"} || missingRequiredOptions(\%options) ) {
	print_usage();
	exit(0);
}

# setup the OCI integration agent
setup_oci_integration_agent(\%options);

# exit with success
exit(0);

#------------------------------------------------------------#
#
# routines beyond this point are "private"
#
#------------------------------------------------------------#

# 
# Check to see if we are missing any required options from command line
# Parameters:
# *) options: the command line parameters
# 
sub missingRequiredOptions($@) {
	my $options = shift;
	return !exists $options->{oci_url} || !exists $options->{oci_user} || !exists $options->{oci_password};
}

# Create or update an OnCommand Insight (OCI) integration agent
# capable of pushing data into defined namespaces.
# Parameters:
# *) options: the command line parameters
sub setup_oci_integration_agent($@) {
	my $options = shift;
	my $url = $options->{oci_url};
	my $agent_token = $options->{oci_agent_token};
	my $username = $options->{oci_user};
	my $password = $options->{oci_password};
	my $namespaces = $options->{oci_namespaces};

	logger "DEBUG", "\nSetting up agent token from info: \n\toci_agent_token: $agent_token\n\turl: $url\n\tuser: $username\n\tnamespaces: $namespaces\n";

	# create user agent for request, bypass SSL verification
	my $ua = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0, SSL_verify_mode => 0 });

	# build URL for call, use OCI's REST API for integration agent

	# the url to access
	my $server_endpoint = $url . '/rest/v1/integrationAgents';
	# the request 
	my $req;
	# the operation string
	my $operation_string;
	if ( $agent_token ) {
		$server_endpoint = $server_endpoint . "/$agent_token";
		$req = HTTP::Request->new(PUT => $server_endpoint);
		$operation_string = "PUT";
	} else {
		$req = HTTP::Request->new(POST => $server_endpoint);
		$operation_string = "POST";
	}

	# create JSON string for call
	my $json = createIntegrationAgentJson($namespaces);

	# debug info
	logger "DEBUG", "\nRequest:\n\tMethod: $operation_string\n\tURL:$server_endpoint\n\tJSON: $json\n";

	# set content type for request
	$req->header('content-type' => 'application/json');
	# set basic authentication header for request
	$req->header('Authorization' => 'Basic ' . encode_base64("$username:$password")); 
  
	# add data to HTTP request body
	$req->content($json);
   
	# fo request, handle response
	my $resp = $ua->request($req);
	if ($resp->is_success) {
		my $message = $resp->decoded_content;
		logger "DEBUG", "\nReceived reply: $message\n";

		# extract agent token id from response
		$agent_token = extractAgentIdFromResponseJson($message);

		# provide information on what params to add to config file
		print "\n\nAdd to configuration file (e.g. netapp-harvest.conf):\n";
		print "\toci_enabled = 1\n";
		print "\toci_url = $url\n";
		print "\toci_agent_token = $agent_token\n";
		print "\toci_namespaces = $namespaces\n\n";
	}
	else {
		# report error
		logger "ERROR", "\nError: HTTP $operation_string error code: " . $resp->code . ", message: " . $resp->message . ", decoded message: " . $resp->decoded_content . "\n";
	}
}

#
# Create JSON for integration agent create/update request
# Parameters:
# *) namespaces: the comma separated list of namespaces to associated with agent
#
sub createIntegrationAgentJson($@) {
	my $namespaces = shift;
	my $joinedNamespaces = '';
	foreach my $oneNamespace (split(',',$namespaces)) {
		if ( length($joinedNamespaces) > 0 ) {
			$joinedNamespaces = $joinedNamespaces . ',';
		}
		$oneNamespace =~ s/:/_/g;
		# prefix namespace with 'harvest_'
		$joinedNamespaces = $joinedNamespaces . '"harvest_' . $oneNamespace . '"';
	}
	return '{ "description": "Integration for NetApp Harvest", "integrations": [' . $joinedNamespaces . '] }';
}

#
# Extract agent id information from OCI response
# Parameters:
# *) json: the json from response
#   
sub extractAgentIdFromResponseJson($@) {
	my $json = shift;
	if ( $json =~ /"tokenId": "(.*?)"/gs ) {
        	return $1;
	}
	die "Could not extract agent id from response: >>$json<<\n";
}

#
# Logging method
# Parameters:
# *) level: the logging level (e.g. DEBUG, ERROR)
# *) message: the logger message to output
sub logger ($@) {
	my $level = shift;
	my $message = shift;
	return if (($level eq 'DEBUG') && (! exists $options{'v'})); # Only log DEBUG if verbose enabled

	print $message;
}

#
# Print script usage
#
sub print_usage
{
print <<EOF;

Usage: $Bin/$Script -oci_url <str> -oci_user <str> -oci_password <str> [-oci_namespaces <str>] [-oci_agent_token <str>] [-h] [-v]

 PURPOSE:
  Configure OnCommand Insight to receive data from Harvest. Creates new 
  agent or updates agent to allow writing to OCI's integrations.
  
 ARGUMENTS:
  Required:
    -oci_url <str>         OnCommand Insight URL (e.g. https://192.168.1.100/)
    -oci_user <str>        Administrator user to create OCI integration
    -oci_password <str>    Administrator password to create OCI integration
  Optional:
    -oci_namespaces <str>  Comma separated list of namespaces to add to agent
    -oci_agent_token <str> The agent to update, not passed in if creating new
    -h                     Output this help text
    -v                     Output verbose output to stdout and logfile
 EXAMPLES:

 Retrieve a brand new OCI agent accessing specified namespaces
  $Script -oci_url https://oci.host.com -oci_user admin -oci_password adminpass -oci_namespaces nfsv3:node,nfsv4:node,nfsv4_1:node

 Update OCI agent acessing a different set of namespaces
  $Script -oci_url https://oci.host.com -oci_user admin -oci_password adminpass -oci_agent_token cffce554-4677-47aa-a8b0-23888aa9f3ca -oci_namespaces nfsv3:node,nfsv4:node

EOF
}

