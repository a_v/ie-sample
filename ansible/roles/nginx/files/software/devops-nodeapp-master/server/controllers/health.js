const PACKAGE = require('./../../package.json');

class Health {
  static async get (ctx, next) {
    ctx.status = 200;
    ctx.body   = {
      OK: true,
      success: true,
      code: ctx.status,
      application: {
        name: PACKAGE.name,
        version: PACKAGE.version
      }
    };

    return ctx.body;
  }
};

module.exports = Health;
