// Node Modules
const MONGOOSE = require('mongoose');
const SEQUAL   = require('sequelize');

// ENV Variables
const ENV  = process.env;

// Injections
MONGOOSE.Promise = require('bluebird');

class Db {
  static initiate (){
    let DB = null;

    if (ENV.DATABASE_OPTION === 'mongo') {
      MONGOOSE.connect(ENV.MONGO_URI);
      MONGOOSE.connection.on('error',  (err) => {
        console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');        
        console.error(err.stack || err);
      });

      DB = MONGOOSE;
    } else {
      const Sequelize = new SEQUAL(ENV.SQL_DATABASE, ENV.SQL_USERNAME, ENV.SQL_PASSWORD, {
        host: ENV.SQL_HOST,
        dialect: ENV.SQL_DIALECT || 'postgres',
        pool: {
          max: 5,
          min: 0,
          acquire: 30000,
          idle: 10000
        }
      });

      DB = Sequelize;
    }

    return DB;
  }
}

module.exports = Db.initiate();
