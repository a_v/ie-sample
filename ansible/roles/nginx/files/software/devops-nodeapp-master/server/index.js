// Node Modules
const KOA      = require('koa');
const CORS     = require('kcors');
const LOGS     = require('koa-logger');

// ENV Variables
const ENV  = process.env;
const PORT = ENV.port || 3000;

// Modular Scope
const DB     = require('./db');
const ROUTER = require('./routes');

class Server {
  constructor () {
    this.db  = null;
    this.app = new KOA(); 
  }

  initialize () {
    this.db = DB;

    this.app.use(LOGS());
    this.app.use(CORS());
    this.app.use(ROUTER.routes());
    this.app.use(ROUTER.allowedMethods());

    this.app.listen(PORT);

    console.log(`Starting service on port: ${PORT}`);
  }

};

let RestService = new Server();
module.exports = RestService.initialize();


