const MONGOOSE = require('mongoose');

const MonitorSchema = new MONGOOSE.Schema({
  name: {type: String},
  status: {type: String}
});

const Monitor = MONGOOSE.model('Monitor', MonitorSchema);

module.exports = Monitor;