# What Am I Doing here 
## Cloud 
I choose AWS as I'm the most proficent there now. us-west-2 is the region

## The Goal 
To create AWS infrastracture for 3 servers 
* A web server that will front end a node application. The node application will use nginx as a revers proxy and poxy request to to port 3000
* A database server that will be postgres that should be a backend for said node application.  
* a server that will be used for some log/moniotring of some kind. ELK maybe.. not sure


## How to use 
``` export AWS_SECRET_KEY=""```

``` export AWS_ACCESS_KEY=""```



you will also need to create a tfvars file with all the variables that you will need. 

``` terraform plan -out="tfplan.out" -var-file="your_tf_var_file" ```

