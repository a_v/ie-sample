/**
 * VPG.
 *
 * Allows our public subnet to get out to the internet
 */
resource "aws_internet_gateway" "internet" {
  vpc_id = "${aws_vpc.vpc.id}"

  tags {
    Name = "Default_Internet_Gateway"
  }
}


/**
 * VPG.
 *
 * Creates external IP for the neat gateway
 */
 
resource "aws_eip" "nat_eip" {
  vpc = true
}
/**
 * VPG.
 *
 * Attatches Creates a NAT Gateway for Priavte networking to reach public 
 */
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.nat_eip.id}"
  subnet_id     = "${aws_subnet.public_2a.id}"
  depends_on = ["aws_internet_gateway.internet"]
}
