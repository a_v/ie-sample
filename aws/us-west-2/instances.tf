
resource "aws_key_pair" "key" {
  key_name   = "instance-key"
  public_key = "${var.public_key}"
}
# Instance 

resource "aws_instance" "web1" {
  ami           = "${var.aws_ami}"
  instance_type = "${var.instance_size}"
  key_name      = "${aws_key_pair.key.id}"
  subnet_id     = "${aws_subnet.private_2a.id}"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]

  tags {
    Name = "Web-Node"
  }
}

resource "aws_instance" "mon1" {
  ami           = "${var.aws_ami}"
  instance_type = "${var.instance_size}"
  key_name      = "${aws_key_pair.key.id}"
  subnet_id     = "${aws_subnet.private_2b.id}"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]
  tags {
    Name = "Monitor-Node"
  }
}
resource "aws_instance" "db1" {
  ami           = "${var.aws_ami}"
  instance_type = "${var.instance_size}"
  key_name      = "${aws_key_pair.key.id}"
  subnet_id     ="${aws_subnet.private_2c.id}"
  vpc_security_group_ids = ["${aws_security_group.default.id}"]

  tags {
    Name = "DB-Node"
  }
}