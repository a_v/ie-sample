/**
 * Outputs
 *
 * This output allows terraform to bring in that value into other states. 
 */

output "public_2a" {
  value = "${aws_subnet.public_2a.id}"
}
output "public_2b" {
  value = "${aws_subnet.public_2b.id}"
}
output "public_2c" {
  value = "${aws_subnet.public_2c.id}"
}
output "private_2a" {
  value = "${aws_subnet.private_2a.id}"
}
output "private_2b" {
  value = "${aws_subnet.private_2b.id}"
}
output "private_2c" {
  value = "${aws_subnet.private_2c.id}"
}
output "vpc" {
  value = "${aws_vpc.vpc.id}"
}
