/* Creating a route that will route Public Subnet traffic to our Internet-Gateway */
resource "aws_route_table" "internet" {
  vpc_id = "${aws_vpc.vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.internet_gateway.id}"
  }

  tags {
    Name = "prod_external"
  }
}

/* Creating a route that will route Private Subnet traffic to our NAT-Gateway */
resource "aws_route_table" "internal" {
  vpc_id = "${aws_vpc.vpc.id}"

  // add your routes here  //
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat_gw.id}"
  }

  tags {
    Name = "internal"
  }
}

/* Associating the subnets to the above route rule so we can move traffic to our VPN/Direct-Connect */

resource "aws_route_table_association" "private_2a" {
  subnet_id      = "${aws_subnet.private_2a.id}"
  route_table_id = "${aws_route_table.internal.id}"
}

resource "aws_route_table_association" "private_2b" {
  subnet_id      = "${aws_subnet.private_2b.id}"
  route_table_id = "${aws_route_table.internal.id}"
}

resource "aws_route_table_association" "private_2c" {
  subnet_id      = "${aws_subnet.private_2c.id}"
  route_table_id = "${aws_route_table.internal.id}"
}


/* Associating the public subnet to the above route to move 0.0.0.0/0 traffic to the internet */
resource "aws_route_table_association" "public_2a" {
  subnet_id      = "${aws_subnet.public_2a.id}"
  route_table_id = "${aws_route_table.internet.id}"
}

resource "aws_route_table_association" "public_2b" {
  subnet_id      = "${aws_subnet.public_2b.id}"
  route_table_id = "${aws_route_table.internet.id}"
}

resource "aws_route_table_association" "public_2c" {
  subnet_id      = "${aws_subnet.public_2c.id}"
  route_table_id = "${aws_route_table.internet.id}"
}
