/**
 * Security group.
 *
 * Basic security group with the following rules:
 *  - Inbound traffic on port 3389 restricted to private IPs.
 *  - Inbound traffic on port 22 unrestricted (enables us to setup instances
 *    and  run tests).
 *  - Outbound traffic unrestricted.
 */

 resource "aws_security_group" "default" {
  name_prefix = "enso-"
  description = "Default security group for all instances in VPC ${aws_vpc.vpc.id}"
  vpc_id      = "${aws_vpc.vpc.id}"
  tags {
    Name ="RDP/SSH"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 8
    to_port     = 0
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

 /*Egress Rule */
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
