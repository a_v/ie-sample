
//variables for the default aws region
variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "us-west-2"
}

//Variables for VPC Ciders
variable "vpc_cidr" {}

//variables for public subnet for enviornment
variable "public_subnet_cidr" {}


//variables for private subnet for enviornment
variable "private_subnet_cidr" {}

variable "aws_az" {}

variable "aws_ami" {}

variable "public_key" {}

variable "instance_size" {}