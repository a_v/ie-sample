/**
 * VPC.
 *
 * Virtual network which will be referred to Enso
 **/
resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpc_cidr}"
  tags {
    Name = "vpc"
  }
}

/**
 * Public Subnet.
 *
 * /24 subnet for availability zone 
 **/
resource "aws_subnet" "public_2a" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnet_cidr.["public_west_2a"]}"
  availability_zone       = "${var.aws_az.["us-west-2a"]}"
  tags {
    Name = "public_2a"
  }
}

resource "aws_subnet" "public_2b" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnet_cidr.["public_west_2b"]}"
  availability_zone       = "${var.aws_az.["us-west-2b"]}"
  tags {
    Name = "public_2b"
  }
}
resource "aws_subnet" "public_2c" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.public_subnet_cidr.["public_west_2c"]}"
  availability_zone       = "${var.aws_az.["us-west-2c"]}"
  tags {
    Name = "public_2c"
  }
}

/**
 * Private Subnet.
 *
 * /24 subnet for availability zones 
 **/

resource "aws_subnet" "private_2a" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnet_cidr.["private_west_2a"]}"
  availability_zone       = "${var.aws_az.["us-west-2a"]}"
  tags {
    Name = "private_2a"
  }
}

resource "aws_subnet" "private_2b" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnet_cidr.["private_west_2b"]}"
  availability_zone       = "${var.aws_az.["us-west-2b"]}"
  tags {
    Name = "private_2b"
  }
}


resource "aws_subnet" "private_2c" {
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "${var.private_subnet_cidr.["private_west_2c"]}"
  availability_zone       = "${var.aws_az.["us-west-2c"]}"
  tags {
    Name = "private_2c"
  }
}
