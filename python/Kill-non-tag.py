# /usr/local/bin/python
#
#
# Script will loop through all the instances and will terminate based on no tags 
#  
#  
import boto3
import sys 
# variables 
aws_acess_key = ''
aws_secret_key = ''
region = 'us-west-2'

client = boto3.client('ec2', aws_access_key_id=aws_acess_key,
                            aws_secret_access_key=aws_secret_key,
                            region_name=region )
ec2 = boto3.resource('ec2', aws_access_key_id=aws_acess_key,
                            aws_secret_access_key=aws_secret_key,
                            region_name=region)

#loops through all the instances in an acount and adds those as intance varriable 
instances = [i for i in ec2.instances.all()]
# Looks for tags that have owner in them. If not it will terminate the instance '
def terminate_no_tag():
    for i in instances:
        try:
            if 'Owner' not in [t['Key'] for t in i.tags]:
                ec2.instances.terminate (i.instance_id)
                print ( i.instance_id + ' terminated for no Owner tag' )
                
            else:
                if 'Owner' in [t['Key'] for t in i.tags]:
                    print ( i.instance_id + ' Has Owner Tag' )       
        except: 
            print ( "test" )


terminate_no_tag()