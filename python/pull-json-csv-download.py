# Script will pull in a json file from a url, parse the json file for keys' you will look for and print them in a coloum and row form. It will also download the icon images and put them in a data folder. 


import json, requests, csv, time, os, urllib.request

response = requests.get("https://skillz.com/games.json")
data = json.loads(response.text)["resultReferences"]
readable = json.dumps(data, sort_keys=True, indent=4 )

'''
#print ( data )

for games in data: 
    print ( games['icon_url'] )


'''

# Write CSV Header, If you dont need that, remove this line
def json_to_csv():
    fname = time.strftime("%Y%m%d-%H%M%S")
    f = csv.writer(open(fname, "w"))
    f.writerow(["id", "title", "icon_url", "Publisher_name", "created_at"])

    for games in data:
        f.writerow([games["id"],
                    games["title"],
                    games["icon_url"],
                    games["publisher"]["name"],
                    games["publisher"]["created_at"]])
    print ( 'Printed CSV File ')
    
def download_images():
    print ('Downloading Images Now')
    timestamp = time.strftime("%Y%m%d-%H%M%S")
    loc_data = "./data/{}".format(timestamp)
    try:
        os.makedirs(loc_data)
    except:
        pass
    iimage = 0
    for icons in data:
        try:
            f = open(loc_data + '_image_{:05.0f}.jpg'.format(iimage),'wb')
            f.write(requests.get(icons['icon_url']).content)
            f.close()
            iimage += 1
        except:
            pass
    print ( 'You are all done' )    

def main():
    json_to_csv()
    download_images()



main()