
# What Am I Doing Here 
## CM 
I choose SALT, more specific salt-ssh, as the CM of choice. I have been using salt most recently and can get around a little better.  

## The Goal 
To Automate  
* The installation of base packages that all servers should get and any additional repo's that the servers might need. 

* Automate the installation of the nodej,npm, and Nginx for the node app. Make sure the nginx config file is managed and also daemonize the node app for restarts.
* Automate the installation and the data configuration of postgres. ( work in progress)


## How to use 
1. Install salt-ssh via whatever method in their documentaoitn that meets your needs. 
2. edit your roster file and create the servers and creds there. Location of the file is /etc/salt/
    - Example 
        - ```
             web2:
                host: 192.168.10.45
                user: adrean.vianna
                sudo: True
                minion_opts:
                  grains:
                    roles:
                      - python 
3. run ```salt-ssh -i web2 ``` 
    - This shold prompt you to install the right keys onto the box 
4.  run whatever the state that you are looking for ``` sudo salt-ssh web1 state.apply nginx ```
