base_packages:     
  pkg.installed:
    - pkgs:
      - epel-release
      - git
      - curl
      - GitPython
      - td-agent

pgdg-centos96-9.6-3.noarch.rpm:    
  pkg.installed:
    - sources:
      - pgdg-centos96-9.6-3.noarch.rpm: https://download.postgresql.org/pub/repos/yum/9.6/redhat/rhel-7-x86_64/pgdg-centos96-9.6-3.noarch.rpm

firewalld:
  pkg.installed: []
  service.dead:
    - enable: false
