/var/devops-nodeapp-master/nodeapp:
  file.directory: []

Moving to /var/devops-nodeapp-master/nodeapp :
  file.recurse:
    - name: /var/devops-nodeapp-master/nodeapp
    - source: salt://nginx/files/devops-nodeapp-master

/etc/systemd/system/node-nodeapp.service:
  file.managed:
    - contents: |
        [Unit]
        After=network.target

        [Service]
        ExecStart=/usr/local/bin/npm start
        WorkingDirectory=/var/devops-nodeapp-master/nodeapp
        Restart=always

        [Install]
        WantedBy=multi-user.target

node-nodeapp-daemon-reload:
  module.run:
    - name: service.systemctl_reload
    - watch:
      - file: /etc/systemd/system/node-nodeapp.service

node-nodeapp-service:
  service.running:
    - name: node-nodeapp
    - enable: True
    - watch:
      - file: /etc/systemd/system/node-nodeapp.service