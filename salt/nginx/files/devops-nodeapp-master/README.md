# Start

### Environment Variables
- config.env

### Running
- npm start

### Endpoints
- [GET] /health
- [GET] /monitor

### Requirements
- node.js 6+
