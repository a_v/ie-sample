
class Blackhole {
  static async get (ctx, next) {
    ctx.status = 404;
    ctx.body   = {
      success: false,
      code: ctx.status,
      error: 'not found'
    };

    return ctx.body;
  }
};

module.exports = Blackhole;
