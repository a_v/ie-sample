const ENV = process.env;

const MonitorModel = require('./../models/monitor/');

class Monitor {
  static async get (ctx, next) {
    let result = (ENV.DATABASE_OPTION === 'mongo') ? await MonitorModel.find() : await MonitorModel.findAll();

    ctx.status = 200;
    ctx.body   = {
      success: true,
      code: ctx.status,
      result: result
    };

    return ctx.body;

  }
};

module.exports = Monitor;
