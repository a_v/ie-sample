const ENV = process.env;

if (ENV.DATABASE_OPTION === 'mongo') {
  module.exports = require('./mongo/');
} else {
  module.exports = require('./sql/');  
}