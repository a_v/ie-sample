const SEQUALIZE     = require('sequelize');
const DB            = require('./../../../db');

const MonitorSchema = DB.define('monitor', {
  name: {
    type: SEQUALIZE.STRING
  },
  status: {
    type: SEQUALIZE.STRING
  },
  createdAt: {
    type: SEQUALIZE.DATE,
    field: 'created_at'
  },
  updatedAt: {
    type: SEQUALIZE.DATE,
    field: 'updated_at'
  }
});


module.exports = MonitorSchema;
