const ROUTER = require('koa-router');

const MONITOR_CONTROLLER    = require('./controllers/monitor');
const HEALTH_CONTROLLER     = require('./controllers/health');
const BLACKHOLE_CONTROLLER  = require('./controllers/blackhole');

class Router {
  static initialize() {
    let router = new ROUTER();

    router.get('/health', HEALTH_CONTROLLER.get);
    router.get('/monitor', MONITOR_CONTROLLER.get);
    router.all('*', BLACKHOLE_CONTROLLER.get);

    return router;
    
  }
}

module.exports = Router.initialize();
