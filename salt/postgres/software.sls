postgres_packages:
  pkg.installed:
    - pkgs:
      - postgresql96-server
      - postgresql96-contrib

setup_initdb:
  cmd.run: 
  - name: /usr/pgsql-9.6/bin/postgresql96-setup initdb
  - require: 
    - pkg: postgres_packages
  - unless: ls /var/lib/pgsql/9.6/data/base

postgresql-9.6:  
  service.running:
    - enable: True
     
devops_database_user:  
  postgres_user.present:
    - name: postgres
    - superuser: True
    - password: complex_p$ssw0rd
    - login: True 
    - require:
      - pkg: postgres_packages

devops_database:  
  postgres_database.present:
    - name: devops
    - require:
      - pkg: postgres_packages

/var/lib/pgsql/9.6/data/pg_hba.conf:
  file.managed:
   - source: salt://postgres/files/etc/postgresql/9.6/main/pg_hba.conf
   - user: root
   - group: root
   - mode: 644